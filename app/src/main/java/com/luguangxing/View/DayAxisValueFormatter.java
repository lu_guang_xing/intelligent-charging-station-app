package com.luguangxing.View;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.formatter.ValueFormatter;

/**
 * 日期格式
 *
 * @author xuexiang
 * @since 2019/4/10 下午11:24
 */
public class DayAxisValueFormatter extends ValueFormatter {

    private final BarLineChartBase<?> chart;

    public DayAxisValueFormatter(BarLineChartBase<?> chart) {
        this.chart = chart;
    }

    @Override
    public String getFormattedValue(float value) {
        int dayOfMonth = (int) value;
        String appendix = "月";
        return dayOfMonth == 0 ? "" : dayOfMonth + appendix;
    }
}
