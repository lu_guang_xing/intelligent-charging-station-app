package com.luguangxing.Bean;

import java.io.Serializable;

public class Login implements Serializable {
    private String status;
    private String token;
    private User data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getData() {
        return data;
    }

    public void setData(User user) {
        this.data = user;
    }
}
