package com.luguangxing.Bean;

public class ProductResultList {
    private Double productId;
    private String productName;
    private String tenantId;
    private String productDesc;
    private Double productType;
    private Double secondaryType;
    private Double thirdType;
    private Double productProtocol;
    private Double authType;
    private Double payloadFormat;
    private Double createTime;
    private Double updateTime;
    private Double networkType;
    private Double endpointFormat;
    private Double powerModel;
    private String apiKey;
    private Double deviceCount;
    private String productTypeValue;
    private String secondaryTypeValue;
    private String thirdTypeValue;
    private Double encryptionType;
    private Double rootCert;
    private String createBy;
    private Double updateBy;
    private String tupDeviceModel;
    private String tupDeviceType;
    private Double accessType;
    private Double nodeType;
    private Double tupIsThrough;
    private Double dataEncryption;
    private Double lwm2mEdrxTime;
    private Double categoryId;
    private Double categoryName;

    public Double getProductId() {
        return productId;
    }

    public void setProductId(Double productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public Double getProductType() {
        return productType;
    }

    public void setProductType(Double productType) {
        this.productType = productType;
    }

    public Double getSecondaryType() {
        return secondaryType;
    }

    public void setSecondaryType(Double secondaryType) {
        this.secondaryType = secondaryType;
    }

    public Double getThirdType() {
        return thirdType;
    }

    public void setThirdType(Double thirdType) {
        this.thirdType = thirdType;
    }

    public Double getProductProtocol() {
        return productProtocol;
    }

    public void setProductProtocol(Double productProtocol) {
        this.productProtocol = productProtocol;
    }

    public Double getAuthType() {
        return authType;
    }

    public void setAuthType(Double authType) {
        this.authType = authType;
    }

    public Double getPayloadFormat() {
        return payloadFormat;
    }

    public void setPayloadFormat(Double payloadFormat) {
        this.payloadFormat = payloadFormat;
    }

    public Double getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Double createTime) {
        this.createTime = createTime;
    }

    public Double getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Double updateTime) {
        this.updateTime = updateTime;
    }

    public Double getNetworkType() {
        return networkType;
    }

    public void setNetworkType(Double networkType) {
        this.networkType = networkType;
    }

    public Double getEndpointFormat() {
        return endpointFormat;
    }

    public void setEndpointFormat(Double endpointFormat) {
        this.endpointFormat = endpointFormat;
    }

    public Double getPowerModel() {
        return powerModel;
    }

    public void setPowerModel(Double powerModel) {
        this.powerModel = powerModel;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Double getDeviceCount() {
        return deviceCount;
    }

    public void setDeviceCount(Double deviceCount) {
        this.deviceCount = deviceCount;
    }

    public String getProductTypeValue() {
        return productTypeValue;
    }

    public void setProductTypeValue(String productTypeValue) {
        this.productTypeValue = productTypeValue;
    }

    public String getSecondaryTypeValue() {
        return secondaryTypeValue;
    }

    public void setSecondaryTypeValue(String secondaryTypeValue) {
        this.secondaryTypeValue = secondaryTypeValue;
    }

    public String getThirdTypeValue() {
        return thirdTypeValue;
    }

    public void setThirdTypeValue(String thirdTypeValue) {
        this.thirdTypeValue = thirdTypeValue;
    }

    public Double getEncryptionType() {
        return encryptionType;
    }

    public void setEncryptionType(Double encryptionType) {
        this.encryptionType = encryptionType;
    }

    public Double getRootCert() {
        return rootCert;
    }

    public void setRootCert(Double rootCert) {
        this.rootCert = rootCert;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Double getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Double updateBy) {
        this.updateBy = updateBy;
    }

    public String getTupDeviceModel() {
        return tupDeviceModel;
    }

    public void setTupDeviceModel(String tupDeviceModel) {
        this.tupDeviceModel = tupDeviceModel;
    }

    public String getTupDeviceType() {
        return tupDeviceType;
    }

    public void setTupDeviceType(String tupDeviceType) {
        this.tupDeviceType = tupDeviceType;
    }

    public Double getAccessType() {
        return accessType;
    }

    public void setAccessType(Double accessType) {
        this.accessType = accessType;
    }

    public Double getNodeType() {
        return nodeType;
    }

    public void setNodeType(Double nodeType) {
        this.nodeType = nodeType;
    }

    public Double getTupIsThrough() {
        return tupIsThrough;
    }

    public void setTupIsThrough(Double tupIsThrough) {
        this.tupIsThrough = tupIsThrough;
    }

    public Double getDataEncryption() {
        return dataEncryption;
    }

    public void setDataEncryption(Double dataEncryption) {
        this.dataEncryption = dataEncryption;
    }

    public Double getLwm2mEdrxTime() {
        return lwm2mEdrxTime;
    }

    public void setLwm2mEdrxTime(Double lwm2mEdrxTime) {
        this.lwm2mEdrxTime = lwm2mEdrxTime;
    }

    public Double getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Double categoryId) {
        this.categoryId = categoryId;
    }

    public Double getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(Double categoryName) {
        this.categoryName = categoryName;
    }
}
