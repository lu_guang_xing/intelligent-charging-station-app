package com.luguangxing.Bean;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 20/5/2023
 * @desc:
 */
public class StationBean implements Serializable {
    private int deviceNum;
    private String deviceImei;
    private String deviceAddress;
    private int interfaceNum;
    private String deviceStatus;
    private float pricePerHour;
    private String deviceComment;

    public StationBean(int deviceNum, String deviceImei, String deviceAddress, int interfaceNum, String deviceStatus, float pricePerHour, String deviceComment) {
        this.deviceNum = deviceNum;
        this.deviceImei = deviceImei;
        this.deviceAddress = deviceAddress;
        this.interfaceNum = interfaceNum;
        this.deviceStatus = deviceStatus;
        this.pricePerHour = pricePerHour;
        this.deviceComment = deviceComment;
    }

    public int getDeviceNum() {
        return deviceNum;
    }

    public void setDeviceNum(int deviceNum) {
        this.deviceNum = deviceNum;
    }

    public String getDeviceImei() {
        return deviceImei;
    }

    public void setDeviceImei(String deviceImei) {
        this.deviceImei = deviceImei;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public int getInterfaceNum() {
        return interfaceNum;
    }

    public void setInterfaceNum(int interfaceNum) {
        this.interfaceNum = interfaceNum;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public float getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(float pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public String getDeviceComment() {
        return deviceComment;
    }

    public void setDeviceComment(String deviceComment) {
        this.deviceComment = deviceComment;
    }

    @Override
    public String toString() {
        return "StationBean{" +
                "deviceNum=" + deviceNum +
                ", deviceImei='" + deviceImei + '\'' +
                ", deviceAddress='" + deviceAddress + '\'' +
                ", interfaceNum=" + interfaceNum +
                ", deviceStatus='" + deviceStatus + '\'' +
                ", pricePerHour=" + pricePerHour +
                ", deviceComment='" + deviceComment + '\'' +
                '}';
    }
}
