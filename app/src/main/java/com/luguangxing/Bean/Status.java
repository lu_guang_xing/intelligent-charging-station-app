package com.luguangxing.Bean;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 2022/11/20
 * @desc:
 */

public class Status implements Serializable {
    String status;
    String code;

    public Status(String status, String code) {
        this.status = status;
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Status{" +
                "status='" + status + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}