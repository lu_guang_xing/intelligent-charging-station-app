package com.luguangxing.Bean;

/**
 * @author: 小白一号
 * @date: 2022/11/21
 * @desc:
 */
public class AppData {
    private boolean isFromLogout;

    public AppData() {
    }

    public AppData(boolean isFromLogout) {
        this.isFromLogout = isFromLogout;
    }

    public boolean isFromLogout() {
        return isFromLogout;
    }

    public void setFromLogout(boolean fromLogout) {
        isFromLogout = fromLogout;
    }
}
