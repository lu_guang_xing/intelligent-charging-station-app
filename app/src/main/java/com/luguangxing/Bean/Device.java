package com.luguangxing.Bean;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 2022/11/24
 * @desc:
 */
public class Device implements Serializable {
    private Drawable deviceIcon;
    private String deviceId;
    private String imei;
    private String location;
    private String status;

    public Device() {
    }

    public Device(Drawable deviceIcon, String deviceId, String imei, String location, String status) {
        this.deviceIcon = deviceIcon;
        this.deviceId = deviceId;
        this.imei = imei;
        this.location = location;
        this.status = status;
    }

    public Drawable getDeviceIcon() {
        return deviceIcon;
    }

    public void setDeviceIcon(Drawable deviceIcon) {
        this.deviceIcon = deviceIcon;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Device{" +
                "deviceId='" + deviceId + '\'' +
                ", imei='" + imei + '\'' +
                ", location='" + location + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
