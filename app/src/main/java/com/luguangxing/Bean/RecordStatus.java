package com.luguangxing.Bean;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 2022/11/22
 * @desc:
 */
public class RecordStatus implements Serializable {
    String month;
    String cost;

    public RecordStatus() {
    }

    public RecordStatus(String month, String cost) {
        this.month = month;
        this.cost = cost;
    }

    public String getMonth() {
        return month;
    }

    public String getCost() {
        return cost;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "GroupByMonth{" +
                "month='" + month + '\'' +
                ", cost='" + cost + '\'' +
                '}';
    }
}
