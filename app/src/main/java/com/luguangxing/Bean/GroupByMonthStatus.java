package com.luguangxing.Bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author: 小白一号
 * @date: 2022/11/18
 * @desc:
 */
public class GroupByMonthStatus implements Serializable {
    String status;
    String code;
    List<RecordStatus> data;

    public GroupByMonthStatus() {
    }

    public GroupByMonthStatus(String status, String code) {
        this.status = status;
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public String getCode() {
        return code;
    }

    public Object getData() {
        return data;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setData(List<RecordStatus> data) {
        data = data;
    }

    @Override
    public String toString() {
        return "StatusEntity{" +
                "status='" + status + '\'' +
                ", code='" + code + '\'' +
                ", Data=" + data +
                '}';
    }
}
