package com.luguangxing.Bean;

public class ProductBean {
    private Double code;
    private String msg;
    private ProductResult result;

    public Double getCode() {
        return code;
    }

    public void setCode(Double code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ProductResult getResult() {
        return result;
    }

    public void setResult(ProductResult result) {
        this.result = result;
    }
}
