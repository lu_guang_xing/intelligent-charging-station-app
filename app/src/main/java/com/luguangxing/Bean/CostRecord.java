package com.luguangxing.Bean;

/**
 * @author: 小白一号
 * @date: 2022/11/23
 * @desc:
 */
public class CostRecord {
    private String deviceId;  //设备号
    private String orderId;   //订单号
    private String date;    //日期
    private float cost;    //消费金额

    public CostRecord() {
    }

    public CostRecord(String deviceId, String orderId, String date, float cost) {
        this.deviceId = deviceId;
        this.orderId = orderId;
        this.date = date;
        this.cost = cost;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "CostRecord{" +
                "deviceId='" + deviceId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", date='" + date + '\'' +
                ", cost=" + cost +
                '}';
    }
}
