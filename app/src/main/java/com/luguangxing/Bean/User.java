package com.luguangxing.Bean;

import java.io.Serializable;

/**
 * @author: 小白一号
 * @date: 2022/11/20
 * @desc:
 */

public class User implements Serializable {
    private String userAccount;
    private String userPassword;
    private String userName;
    private int userAge;
    private String userSex;
    private String userCardId;
    private float userCardBalance;
    private String userEmail;
    private String userPhone;

    public User() {
        userAccount = "";
        userPassword = "";
        userName = "";
        userAge = 0;
        userSex = "";
        userCardId = "";
        userCardBalance = 0f;
        userEmail = "";
        userPhone = "";
    }

    public User(String userAccount, String userPassword, String userName, int userAge, String userSex, String userCardId, float userCardBalance, String userEmail, String userPhone) {
        this.userAccount = userAccount;
        this.userPassword = userPassword;
        this.userName = userName;
        this.userAge = userAge;
        this.userSex = userSex;
        this.userCardId = userCardId;
        this.userCardBalance = userCardBalance;
        this.userEmail = userEmail;
        this.userPhone = userPhone;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserAge() {
        return userAge;
    }

    public void setUserAge(int userAge) {
        this.userAge = userAge;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public String getUserCardId() {
        return userCardId;
    }

    public void setUserCardId(String userCardId) {
        this.userCardId = userCardId;
    }

    public float getUserCardBalance() {
        return userCardBalance;
    }

    public void setUserCardBalance(float userCardBalance) {
        this.userCardBalance = userCardBalance;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    @Override
    public String toString() {
        return "User{" +
                "userAccount='" + userAccount + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userName='" + userName + '\'' +
                ", userAge=" + userAge +
                ", userSex='" + userSex + '\'' +
                ", userCardId='" + userCardId + '\'' +
                ", userCardBalance=" + userCardBalance +
                ", userEmail='" + userEmail + '\'' +
                ", userPhone='" + userPhone + '\'' +
                '}';
    }
}
