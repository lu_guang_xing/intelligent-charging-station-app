package com.luguangxing.Bean;


import java.util.List;
import java.util.Map;

public class ProductResult {
    private Double pageNum;
    private Double pageSize;
    private Double total;
    private List<ProductResultList> list;

    public Double getPageNum() {
        return pageNum;
    }

    public void setPageNum(Double pageNum) {
        this.pageNum = pageNum;
    }

    public Double getPageSize() {
        return pageSize;
    }

    public void setPageSize(Double pageSize) {
        this.pageSize = pageSize;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<ProductResultList> getList() {
        return list;
    }

    public void setList(List<ProductResultList> list) {
        this.list = list;
    }
}