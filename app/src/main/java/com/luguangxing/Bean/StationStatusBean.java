package com.luguangxing.Bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author: 小白一号
 * @date: 20/5/2023
 * @desc:
 */
public class StationStatusBean implements Serializable {
    String status;
    String code;
    String msg;
    String token;
    private List<StationBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<StationBean> getData() {
        return data;
    }

    public void setData(List<StationBean> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "StatusBean{" +
                "status='" + status + '\'' +
                ", code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", token='" + token + '\'' +
                ", data=" + data +
                '}';
    }
}
