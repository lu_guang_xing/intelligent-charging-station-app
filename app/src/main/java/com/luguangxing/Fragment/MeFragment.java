package com.luguangxing.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.luguangxing.Activity.*;
import com.luguangxing.Activity.databinding.FragmentMeBinding;
import com.luguangxing.Interface.OkSuccessInterface;
import com.luguangxing.Util.CommonUtils;
import com.xuexiang.xui.utils.XToastUtils;
import com.xuexiang.xui.widget.popupwindow.ViewTooltip;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.luguangxing.Util.CommonUtils.*;

public class MeFragment extends Fragment {
    private FragmentMeBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentMeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        //GetData();
        try {
            binding.tvUserName.setText(Objects.requireNonNull(GetUserInfo()).getUserName());
            binding.tvUserCardId.setText("卡号：" + (Objects.requireNonNull(GetUserCardId()).isEmpty() ? "未绑定" : GetUserCardId()));
            Map<String, Object> map = new HashMap<>();
            map.put("userAccount", CommonUtils.GetUserInfo().getUserAccount());
            OKJsonPost(getActivity(), new Gson().toJson(map), getString(R.string.get_user_balance_by_user_account), new OkSuccessInterface() {
                @Override
                public void OnSuccess(String json) {
                    Map<String, Object> map = new HashMap<>();
                    map = new Gson().fromJson(json, map.getClass());
                    if (map.get("status").equals("success")) {
                        binding.tvMyBalance.setText("余额：" + map.get("userBalance").toString() + "元");
                    } else if (map.get("status").equals("error")) {
                        XToastUtils.info("用户余额失败！");
                    }
                }
            });
        } catch (Exception e) {

        }
        //CommonUtils.setImageByUrl(binding.headImage, "https://c-ssl.duitang.com/uploads/item/202006/07/20200607210512_rnkiq.jpg");

        /**
         * 我的水卡
         */
        binding.myCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MyCardActivity.class);
                startActivity(intent);
            }
        });

        /**
         * 我的账单
         */
        binding.myBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MyBillActivity.class);
                startActivity(intent);
            }
        });

        /**
         * 修改个人信息
         */
        binding.editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), EditProfileActivity.class);
                startActivity(intent);
            }
        });

        /**
         * 绑定卡片
         */
        binding.about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), CardBinding.class);
                startActivity(intent);
            }
        });

        /**
         * 注销登录
         */
        binding.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                XToastUtils.warning("请重新登录！");
                Intent intent = new Intent(getContext(), LoginActivity.class);
                CommonUtils.SaveKey("UserInfo", "isFromLogout", "true");
                startActivity(intent);
                getActivity().finish();
            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}