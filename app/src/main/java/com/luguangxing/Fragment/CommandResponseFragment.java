//package com.luguangxing.Fragment;
//
//import static com.luguangxing.Util.CommonUtils.OKJsonPost;
//
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.StringRes;
//import androidx.fragment.app.Fragment;
//import androidx.recyclerview.widget.LinearLayoutManager;
//
//import com.codbking.calendar.CalendarDate;
//import com.codbking.calendar.CalendarUtils;
//import com.google.gson.Gson;
//import com.luguangxing.Activity.R;
//import com.luguangxing.Activity.databinding.FragmentCommandResponseBinding;
//
//import com.luguangxing.Bean.CostRecordStatus;
//import com.luguangxing.Interface.OkSuccessInterface;
//import com.luguangxing.Util.CommonUtils;
//import com.scwang.smartrefresh.layout.api.RefreshLayout;
//import com.xuexiang.xui.utils.DensityUtils;
//import com.xuexiang.xui.utils.XToastUtils;
//
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//public class CommandResponseFragment extends Fragment {
//    private FragmentCommandResponseBinding binding;
//    private LinearLayoutManager linearLayoutManager;
////    private CommandResponseAdapter messageAdapter;
//    private RefreshLayout refreshLayout;
//    String today;
//    String tomorrow;
//    String thisMonth;
//    String nextMonth;
//
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        binding = FragmentCommandResponseBinding.inflate(inflater, container, false);
//        View root = binding.getRoot();
//        binding.recyclerviewMessage.setLayoutManager(linearLayoutManager = new LinearLayoutManager(getContext()));
//        binding.tvTitle.setText(CommonUtils.getCurrentTime("yyyy-MM-dd"));
//
//        binding.calendarDateView.setAdapter((convertView, parentView, calendarDate) -> {
//            TextView textView;
//            if (convertView == null) {
//                convertView = LayoutInflater.from(parentView.getContext()).inflate(R.layout.adapter_calendar_item, null);
//                ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(DensityUtils.dp2px(getContext(), 48), DensityUtils.dp2px(getContext(), 48));
//                convertView.setLayoutParams(params);
//            }
//            textView = convertView.findViewById(R.id.tv_text);
//            textView.setBackgroundResource(R.drawable.bg_calendar_ding_ding_item);
//            textView.setText(String.valueOf(calendarDate.day));
//            if (calendarDate.monthFlag != 0) {
//                textView.setTextColor(0xFF9299A1);
//            } else {
//                textView.setTextColor(0xFFFFFFFF);
//            }
//            return convertView;
//        });
//
//        binding.calendarDateView.setOnCalendarSelectedListener((view, position, calendarDate) -> listByDay(calendarDate));
//
//        binding.calendarDateView.setOnMonthChangedListener((view, postion, date) -> listByMonth(date));
//
//        refreshLayout = binding.refreshLayout;
//        // 开启自动加载功能（非必须）
//        refreshLayout.setEnableAutoLoadMore(true);
//        // 下拉刷新
//        Map<String, Object> map = new HashMap<>();
//        map.put("startTime", CommonUtils.getCurrentTime("yyyyMMdd000000"));
//        map.put("endTime", CommonUtils.getTodayAfterday("yyyyMMdd000000", 1));
//        OKJsonPost(getActivity(), new Gson().toJson(map), getString(R.string.find_daily_cost_command_response), new OkSuccessInterface() {
//            @Override
//            public void OnSuccess(String json) {
//                CostRecordStatus status = new Gson().fromJson(json, CostRecordStatus.class);
//                if (status.getStatus().equals("success")) {
//                    messageAdapter = new CommandResponseAdapter(getContext(), (List<Map<String, Object>>) status.getData());
//                    binding.recyclerviewMessage.setAdapter(messageAdapter);
//                } else if (status.getStatus().equals("error")) {
//                    XToastUtils.warning("数据获取失败！");
//                }
//            }
//        });
//        refreshLayout.setOnRefreshListener(refreshLayout12 -> refreshLayout12.getLayout().postDelayed(() -> {
//            refreshLayout12.finishRefresh();
//            refreshLayout12.resetNoMoreData();//setNoMoreData(false);
//        }, 500));
//        // 触发自动刷新
//        refreshLayout.setEnableOverScrollBounce(true);
//        refreshLayout.autoRefresh();
//        return root;
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        binding = null;
//        messageAdapter = null;
//        linearLayoutManager = null;
//    }
//
//    private void listByDay(CalendarDate calendarDate) {
//        binding.tvTitle.setText(String.format("%d-%d-%d", calendarDate.year, calendarDate.month, calendarDate.day));
//        today = String.format("%d%02d%02d000000", calendarDate.year, calendarDate.month, calendarDate.day);
//        tomorrow = String.format("%d%02d%02d000000", calendarDate.year, calendarDate.month, calendarDate.day + 1);
//        // 触发自动刷新
//        refreshLayout.autoRefresh();
//        Date newDate = calendarDate.toDate();
//        Map<String, Object> map = new HashMap<>();
//        map.put("startTime", today);
//        map.put("endTime", tomorrow);
//        OKJsonPost(getActivity(), new Gson().toJson(map), getString(R.string.find_daily_cost_command_response), new OkSuccessInterface() {
//            @Override
//            public void OnSuccess(String json) {
//                CostRecordStatus status = new Gson().fromJson(json, CostRecordStatus.class);
//                if (status.getStatus().equals("success")) {
//                    CommandResponseAdapter messageAdapter = new CommandResponseAdapter(getContext(), (List<Map<String, Object>>) status.getData());
//                    binding.recyclerviewMessage.setAdapter(messageAdapter);
//                } else if (status.getStatus().equals("error")) {
//                    XToastUtils.warning("数据获取失败！");
//                }
//            }
//        });
//    }
//
//    private void listByMonth(CalendarDate calendarDate) {
//        binding.tvTitle.setText(String.format("%d-%d-%d", calendarDate.year, calendarDate.month, calendarDate.day));
//        thisMonth = String.format("%d%02d01000000", calendarDate.year, calendarDate.month, calendarDate.day);
//        nextMonth = String.format("%d%02d01000000", calendarDate.year, calendarDate.month + 1, calendarDate.day);
//        Date newDate = calendarDate.toDate();
//        Map<String, Object> map = new HashMap<>();
//        map.put("startTime", thisMonth);
//        map.put("endTime", nextMonth);
//        OKJsonPost(getActivity(), new Gson().toJson(map), getString(R.string.find_month_cost_command_response), new OkSuccessInterface() {
//            @Override
//            public void OnSuccess(String json) {
//                CostRecordStatus status = new Gson().fromJson(json, CostRecordStatus.class);
//                if (status.getStatus().equals("success")) {
//                    CommandResponseAdapter messageAdapter = new CommandResponseAdapter(getContext(), (List<Map<String, Object>>) status.getData());
//                    binding.recyclerviewMessage.setAdapter(messageAdapter);
//                } else if (status.getStatus().equals("error")) {
//                    XToastUtils.warning("数据获取失败！");
//                }
//            }
//        });
//    }
//
//    private enum Item {
//        BezierCircleHeader(R.string.item_head_style_bezier_circle),
//        DeliveryHeader(R.string.item_head_style_delivery),
//        DropBoxHeader(R.string.item_head_style_drop_box),
//        FunGameBattleCityHeader(R.string.item_head_style_fun_game_battle_city),
//        FunGameHitBlockHeader(R.string.item_head_style_fun_game_hit_block),
//        PhoenixHeader(R.string.item_head_style_phoenix),
//        StoreHouseHeader(R.string.item_head_style_store_house),
//        TaurusHeader(R.string.item_head_style_taurus),
//        WaterDropHeader(R.string.item_head_style_water_drop),
//        WaveSwipeHeader(R.string.item_head_style_wave_swipe);
//
//        public int nameId;
//
//        Item(@StringRes int nameId) {
//            this.nameId = nameId;
//        }
//    }
//
//}