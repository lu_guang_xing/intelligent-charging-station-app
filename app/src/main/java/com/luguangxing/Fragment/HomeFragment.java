package com.luguangxing.Fragment;

import static com.luguangxing.Util.CommonUtils.OKJsonPost;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.luguangxing.Activity.CustomCaptureActivity;
import com.luguangxing.Activity.R;
import com.luguangxing.Activity.databinding.FragmentHomeBinding;
import com.luguangxing.Adapter.DeviceAdapter;
import com.luguangxing.Bean.StationBean;
import com.luguangxing.Bean.StationStatusBean;
import com.luguangxing.Interface.OkSuccessInterface;
import com.xuexiang.xui.utils.XToastUtils;

import java.util.List;

public class HomeFragment extends Fragment {
    private FragmentHomeBinding binding;
    /**
     * 定制化扫描界面Request Code
     */
    public static final int REQUEST_CUSTOM_SCAN = 113;
    private DeviceAdapter deviceAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        OKJsonPost(getActivity(), "", getString(R.string.query_all_charging_station), new OkSuccessInterface() {
            @Override
            public void OnSuccess(String json) {
                StationStatusBean statusBean = new Gson().fromJson(json, StationStatusBean.class);
                List<StationBean> list = (List) statusBean.getData();
//                    list.add(new Device(getResources().getDrawable(R.drawable.ic_sds_device), "土壤温湿度-A1", "867572059039024", "梧州学院大棚种植实验A区", "在线"));
                deviceAdapter = new DeviceAdapter(getContext(), list);

                binding.recyclerviewDevice.setLayoutManager(new LinearLayoutManager(getContext()));
                binding.recyclerviewDevice.setAdapter(deviceAdapter);
            }
        });

        binding.icScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomCaptureActivity.start(getActivity(), 111, R.style.XQRCodeTheme_Custom);
            }
        });

        return root;
    }

    @Override
    public void onResume() {
//        XToastUtils.info("HomeFragment onResume");
        super.onResume();
//        if (deviceAdapter != null) {
//            deviceAdapter.notifyDataSetChanged();
//        }
    }

    @Override
    public void onPause() {
//        XToastUtils.info("HomeFragment onPause");
        super.onPause();
    }

    @Override
    public void onDestroyView() {
//        XToastUtils.info("HomeFragment onDestroyView");
        binding.recyclerviewDevice.getAdapter();
        binding.recyclerviewDevice.setAdapter(null);
        deviceAdapter = null;
        binding = null;
        super.onDestroyView();
    }
}
