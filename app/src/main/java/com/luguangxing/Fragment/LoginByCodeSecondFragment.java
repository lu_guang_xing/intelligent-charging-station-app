package com.luguangxing.Fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.luguangxing.Activity.R;
import com.luguangxing.Activity.databinding.ActivityLoginByCodeBinding;

public class LoginByCodeSecondFragment extends Fragment {
    private ActivityLoginByCodeBinding binding;


    public static LoginByCodeSecondFragment newInstance() {
        return new LoginByCodeSecondFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_by_code_second, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // TODO: Use the ViewModel
    }
}