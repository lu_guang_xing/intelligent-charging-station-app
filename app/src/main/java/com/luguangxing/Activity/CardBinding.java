package com.luguangxing.Activity;

import static com.luguangxing.Util.CommonUtils.GetUserCardId;
import static com.luguangxing.Util.CommonUtils.GetUserInfo;
import static com.luguangxing.Util.CommonUtils.OKJsonPost;
import static com.luguangxing.Util.CommonUtils.SaveKey;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.google.gson.Gson;
import com.kongzue.dialogx.dialogs.MessageDialog;
import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.dialogs.WaitDialog;
import com.kongzue.dialogx.interfaces.DialogLifecycleCallback;
import com.kongzue.dialogx.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialogx.util.TextInfo;
import com.luguangxing.Activity.databinding.ActivityCardBindingBinding;
import com.luguangxing.Interface.OkSuccessInterface;
import com.xuexiang.xui.utils.StatusBarUtils;
import com.xuexiang.xui.utils.XToastUtils;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CardBinding extends AppCompatActivity {
    private ActivityCardBindingBinding binding;
    Map<String, Object> map;
    private boolean isBinded = false;
    private String host = "tcp://43.138.179.149:1883";
    private String userName = "admin";
    private String passWord = "whiteone@lu";
    private String mqtt_id = "mqttdemo11";
    private String mqtt_pub_topic = "ToDevice/";
    private String mqtt_sub_topic = "toAndroidCardBinding/867572059039024";

    private Handler handler;
    private MqttClient client;
    private MqttConnectOptions options;
    private ScheduledExecutorService scheduler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_binding);
        binding = ActivityCardBindingBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        //设置沉浸式状态栏
        StatusBarUtils.translucent(this);
        //设置黑色字体状态栏
        StatusBarUtils.setStatusBarLightMode(this);
        String cardId = Objects.requireNonNull(GetUserCardId());
        map = new HashMap<>();
        if (cardId != null && !cardId.isEmpty()) {
            isBinded = true;
            binding.cardId.setText(cardId);
            binding.cardBindingOk.setVisibility(View.VISIBLE);
        } else {
            isBinded = false;
            binding.cardId.setText("您的卡号");
            binding.cardBindingOk.setEnabled(false);
            binding.cardBindingOk.setVisibility(View.INVISIBLE);
        }

        binding.cardBindingOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //取消绑定
                MessageDialog.show("确认要取消绑定?", "\n\n", "确认", "取消")
                        .setOkTextInfo(new TextInfo().setFontColor(getResources().getColor(R.color.dialogxIOSBlue)))
                        //.setTitleIcon(R.drawable.ic_incorrect)
                        .setOkButton(new OnDialogButtonClickListener<MessageDialog>() {
                            @Override
                            public boolean onClick(MessageDialog baseDialog, View v) {
                                Map<String, Object> map = new HashMap<>();
                                map.put("condition", "userAccount");
                                map.put("conditionValue", Objects.requireNonNull(GetUserInfo()).getUserAccount().toString());
                                map.put("updateWhat", "userCardId");
                                map.put("updateValue", "");
                                SaveKey("UserInfo", "userCardId", map.get("updateValue").toString());
                                OKJsonPost(CardBinding.this, new Gson().toJson(map), getString(R.string.update_profile), new OkSuccessInterface() {
                                    @Override
                                    public void OnSuccess(String json) {
                                        Map<String, Object> map = new HashMap<>();
                                        map = new Gson().fromJson(json, map.getClass());
                                        if (map.get("status").equals("success")) {
                                            TipDialog.show("操作成功", WaitDialog.TYPE.SUCCESS, 500).setDialogLifecycleCallback(new DialogLifecycleCallback<WaitDialog>() {
                                                @Override
                                                public void onDismiss(WaitDialog dialog) {
                                                    binding.cardBindingOk.setVisibility(View.INVISIBLE);
                                                    binding.cardId.setText("您的卡号");
                                                    super.onDismiss(dialog);
                                                }
                                            });
                                        } else if (map.get("status").equals("error")) {
                                            TipDialog.show("操作失败", WaitDialog.TYPE.SUCCESS, 500).setDialogLifecycleCallback(new DialogLifecycleCallback<WaitDialog>() {
                                                @Override
                                                public void onDismiss(WaitDialog dialog) {
                                                    super.onDismiss(dialog);
                                                }
                                            });
                                        }
                                    }
                                });
                                return false;
                            }
                        })
                        .setCancelButton(new OnDialogButtonClickListener<MessageDialog>() {
                            @Override
                            public boolean onClick(MessageDialog baseDialog, View v) {
                                return false;
                            }
                        });
                isBinded = !isBinded;
            }
        });

        init();
        startReconnect();
        handler = new Handler() {
            @SuppressLint({"SetTextIl8n", "HandlerLeak"})
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1: //开机校验更新回传
                        break;
                    case 2: //反馈回转
                        break;
                    case 3: //MQTT收到消息回传
                        map.clear();
                        String body = msg.obj.toString();
                        String card_id = body.substring(body.indexOf("card_id") + 10, body.indexOf('}', body.indexOf("card_id")) - 1);
                        MessageDialog.show("搜寻到一张卡", "\n请确认 " + card_id + " 是您的!\n", "确认", "取消")
                                .setOkTextInfo(new TextInfo().setFontColor(getResources().getColor(R.color.dialogxIOSBlue)))
                                //.setTitleIcon(R.drawable.ic_incorrect)
                                .setOkButton(new OnDialogButtonClickListener<MessageDialog>() {
                                    @Override
                                    public boolean onClick(MessageDialog baseDialog, View v) {
                                        binding.cardId.setText(card_id);
                                        binding.cardBindingOk.setEnabled(true);
                                        Map<String, Object> map = new HashMap<>();
                                        map.put("condition", "userAccount");
                                        map.put("conditionValue", Objects.requireNonNull(GetUserInfo()).getUserAccount().toString());
                                        map.put("updateWhat", "userCardId");
                                        map.put("updateValue", card_id);
                                        SaveKey("UserInfo", "userCardId", map.get("updateValue").toString());
                                        OKJsonPost(CardBinding.this, new Gson().toJson(map), getString(R.string.update_profile), new OkSuccessInterface() {
                                            @Override
                                            public void OnSuccess(String json) {
                                                Map<String, Object> map = new HashMap<>();
                                                map = new Gson().fromJson(json, map.getClass());
                                                if (map.get("status").equals("success")) {
                                                    TipDialog.show("绑定成功", WaitDialog.TYPE.SUCCESS, 500).setDialogLifecycleCallback(new DialogLifecycleCallback<WaitDialog>() {
                                                        @Override
                                                        public void onDismiss(WaitDialog dialog) {
                                                            binding.cardBindingOk.setVisibility(View.VISIBLE);
                                                            super.onDismiss(dialog);
                                                        }
                                                    });
                                                } else if (map.get("status").equals("error")) {
                                                    TipDialog.show("绑定失败", WaitDialog.TYPE.SUCCESS, 500).setDialogLifecycleCallback(new DialogLifecycleCallback<WaitDialog>() {
                                                        @Override
                                                        public void onDismiss(WaitDialog dialog) {
                                                            super.onDismiss(dialog);
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                        return false;
                                    }
                                })
                                .setCancelButton(new OnDialogButtonClickListener<MessageDialog>() {
                                    @Override
                                    public boolean onClick(MessageDialog baseDialog, View v) {
                                        return false;
                                    }
                                });
                        break;
                    case 30: //连接失败
                        XToastUtils.info("连接失败");
                        TipDialog.show("连接失败!", WaitDialog.TYPE.WARNING);
                        break;
                    case 31: //连接成功
                        try {
                            client.subscribe(mqtt_sub_topic, 0);//订阅
                        } catch (MqttException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        break;
                }
            }
        };
    }

    private void init() {
        try {
            //host为主机名，test为clientid即连接MQTT的客户端ID，一般以客户端唯一标识符表示，MemoryPersistence设置clientid的保存形式，默认为以内存保存
            client = new MqttClient(host, mqtt_id,
                    new MemoryPersistence());
            //MQTT的连接设置
            options = new MqttConnectOptions();
            //设置是否清空session,这里如果设置为false表示服务器会保留客户端的连接记录，这里设置为true表示每次连接到服务器都以新的身份连接
            options.setCleanSession(true);
            //设置连接的用户名
            options.setUserName(userName);
            //设置连接的密码
            options.setPassword(passWord.toCharArray());
            // 设置超时时间 单位为秒
            options.setConnectionTimeout(10);
            // 设置会话心跳时间 单位为秒 服务器会每隔1.5*20秒的时间向客户端发送个消息判断客户端是否在线，但这个方法并没有重连的机制
            options.setKeepAliveInterval(20);
            //设置回调
            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                    //连接丢失后，一般在这里面进行重连
                    System.out.println("connectionLost----------");
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    //publish后会执行到这里
                    System.out.println("deliveryComplete---------"
                            + token.isComplete());
                }

                @Override
                public void messageArrived(String topicName, MqttMessage message)
                        throws Exception {
                    //subscribe后得到的消息会执行到这里面
                    System.out.println("messageArrived----------");
                    Message msg = new Message();
                    msg.what = 3;
                    msg.obj = message.toString();
                    handler.sendMessage(msg);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Mqtt_connect() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (!(client.isConnected())) {
                        client.connect(options);
                        Message msg = new Message();
                        msg.what = 31;
                        handler.sendMessage(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Message msg = new Message();
                    msg.what = 30;
                    handler.sendMessage(msg);
                }
            }
        }).start();
    }

    private void startReconnect() {
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (!client.isConnected()) {
                    Mqtt_connect();
                }
            }
        }, 0 * 1000, 10 * 1000, TimeUnit.MILLISECONDS);
    }

    private void publishMessage(String topic, String message2) {
        if (client == null || !client.isConnected()) {
            return;
        }
        MqttMessage message = new MqttMessage();
        message.setPayload(message2.getBytes());
        try {
            client.publish(topic, message);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        client = null;
        handler = null;
        super.onDestroy();
        if (map != null) {
            map.clear();
            map = null;
        }
        if (binding != null) {
            binding = null;
        }
    }
}