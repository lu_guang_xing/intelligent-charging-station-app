package com.luguangxing.Activity;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.google.gson.Gson;
import com.luguangxing.Activity.databinding.ActivityLoginByCodeBinding;
import com.luguangxing.Bean.Login;
import com.luguangxing.Interface.OkSuccessInterface;
import com.luguangxing.Util.CommonUtils;
import com.xuexiang.xpage.utils.GsonUtils;
import com.xuexiang.xui.utils.CountDownButtonHelper;
import com.xuexiang.xui.utils.StatusBarUtils;
import com.xuexiang.xui.utils.XToastUtils;

import java.util.HashMap;
import java.util.Map;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.luguangxing.Util.CommonUtils.OKJsonPost;
import static com.luguangxing.Util.CommonUtils.SaveKey;

public class LoginByCodeActivity extends AppCompatActivity {
    private ActivityLoginByCodeBinding binding;
    private boolean isVerified;
    private String whatTextType;
    private CountDownButtonHelper mCountDownHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_by_code);
        binding = ActivityLoginByCodeBinding.inflate(getLayoutInflater());
        //设置沉浸式状态栏
        StatusBarUtils.translucent(this);
        //设置黑色字体状态栏
        StatusBarUtils.setStatusBarLightMode(this);
        setContentView(binding.getRoot());
        mCountDownHelper = new CountDownButtonHelper(binding.btnGetVerifyCode, 60);

        /**
         * 返回密码登录
         */
        binding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginByCodeActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        /**
         * 获取验证码
         */
        binding.btnGetVerifyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.etUserPhoneOrEmail.getText().toString().isEmpty()) {
                    XToastUtils.info("请填写手机号或邮箱！");
                } else {
                    mCountDownHelper.start();
                    binding.btnGetVerifyCode.setBackgroundColor(getResources().getColor(R.color.lightsteelblue));
                    if (CommonUtils.isEmail(binding.etUserPhoneOrEmail.getText().toString())) {
                        whatTextType = "邮箱";
                        Map<String, Object> map = new HashMap<>();
                        map.put("userEmail", binding.etUserPhoneOrEmail.getText().toString());
                        OKJsonPost(LoginByCodeActivity.this, new Gson().toJson(map), getString(R.string.get_email_code), new OkSuccessInterface() {
                            @Override
                            public void OnSuccess(String json) {
                                Map<String, Object> map = new HashMap<>();
                                map = new Gson().fromJson(json, map.getClass());
                                if (map.get("status").equals("success")) {
                                    XToastUtils.success("验证码已发送到您的邮箱！");
                                }
                            }
                        });
                    } else if (CommonUtils.isTelPhoneNumber(binding.etUserPhoneOrEmail.getText().toString())) {
                        whatTextType = "手机号";
                        Map<String, Object> map = new HashMap<>();
                        map.put("userPhone", binding.etUserPhoneOrEmail.getText().toString());
                        OKJsonPost(LoginByCodeActivity.this, new Gson().toJson(map), getString(R.string.get_phone_code), new OkSuccessInterface() {
                            @Override
                            public void OnSuccess(String json) {
                                Map<String, Object> map = new HashMap<>();
                                map = new Gson().fromJson(json, map.getClass());
                                if (map.get("status").equals("success")) {
                                    XToastUtils.success("验证码已发送到您的手机号！");
                                }
                            }
                        });
                    } else {
                        XToastUtils.info("您输入正确的手机号或邮箱！");
                    }
                }
            }
        });


        /**
         * 判断输入的验证码是否正确
         */
        binding.etVerifyCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.etVerifyCode.getText().length() == 4) {
                    binding.ivVerify.setVisibility(VISIBLE);
                    Map<String, Object> map = new HashMap<>();
                    map.put("phoneOrEmail", binding.etUserPhoneOrEmail.getText().toString());
                    map.put("code", binding.etVerifyCode.getText().toString());
                    OKJsonPost(LoginByCodeActivity.this, new Gson().toJson(map), getString(R.string.verify_code), new OkSuccessInterface() {
                        @Override
                        public void OnSuccess(String json) {
                            Map<String, Object> map = new HashMap<>();
                            map = new Gson().fromJson(json, map.getClass());
                            if (map.get("status").equals("success")) {
                                binding.ivVerify.setImageResource(R.drawable.ic_correct);
                                isVerified = true;
                            } else {
                                isVerified = false;
                                binding.ivVerify.setImageResource(R.drawable.ic_issue);
                            }
                        }
                    });
                } else if (binding.etVerifyCode.getText().length() == 0) {
                    binding.ivVerify.setVisibility(INVISIBLE);
                    isVerified = false;
                } else if (binding.etVerifyCode.getText().length() > 4 ||
                        binding.etVerifyCode.getText().length() < 4) {
                    binding.ivVerify.setVisibility(VISIBLE);
                    isVerified = false;
                    binding.ivVerify.setImageResource(R.drawable.ic_issue);
                }
            }
        });

        /**
         * 登 录
         */
        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.etUserPhoneOrEmail.getText().toString().isEmpty()) {
                    XToastUtils.info("手机号或邮箱为空！");
                } else {
                    String API = "";
                    binding.btnLogin.setText("正在登录。。。");
                    binding.btnLogin.setEnabled(false);
                    Map<String, Object> map = new HashMap<>();
                    map.put("code", binding.etVerifyCode.getText().toString());
                    if (whatTextType.equals("邮箱")) {
                        API = getString(R.string.login_by_email_code);
                        map.put("userEmail", binding.etUserPhoneOrEmail.getText().toString());
                    } else if (whatTextType.equals("手机号")) {
                        API = getString(R.string.login_by_phone_code);
                        map.put("userPhone", binding.etUserPhoneOrEmail.getText().toString());
                    }
                    OKJsonPost(LoginByCodeActivity.this, new Gson().toJson(map), API, new OkSuccessInterface() {
                        @Override
                        public void OnSuccess(String json) {
                            Login loginBean = new Gson().fromJson(json, Login.class);
                            if (loginBean.getStatus().equals("success")) {
                                XToastUtils.success("登录成功!");
                                SaveKey("UserInfo", "info", GsonUtils.toJson(loginBean.getData()));
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                                finish();
                            } else if (loginBean.getStatus().equals("userNoExist")) {
                                XToastUtils.warning("用户不存在！");
                            }
                        }
                    });
                    binding.btnLogin.setEnabled(true);
                    binding.btnLogin.setText("登  录");
                }
            }
        });

        binding.etUserPhoneOrEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (CommonUtils.isEmail(binding.etUserPhoneOrEmail.getText().toString())) {
                    whatTextType = "邮箱";
                } else if (CommonUtils.isTelPhoneNumber(binding.etUserPhoneOrEmail.getText().toString())) {
                    whatTextType = "手机号";
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginByCodeActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCountDownHelper.cancel();
    }

}