package com.luguangxing.Activity;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.google.gson.Gson;
import com.luguangxing.Activity.databinding.ActivityEditProfileBinding;
import com.luguangxing.Interface.OkSuccessInterface;
import com.xuexiang.xui.utils.StatusBarUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.luguangxing.Util.CommonUtils.GetUserInfo;
import static com.luguangxing.Util.CommonUtils.OKJsonPost;

public class EditProfileActivity extends AppCompatActivity {
    private ActivityEditProfileBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        init();
        initEvents();
    }

    private void init() {
        //设置沉浸式状态栏
        StatusBarUtils.translucent(this);
        //设置黑色字体状态栏
        StatusBarUtils.setStatusBarLightMode(this);
    }

    private void initEvents() {
        /**
         * 确认修改按钮点击事件
         */
        binding.btnEdit.setOnClickListener(v -> {
            if (binding.etNickName.getText().toString().isEmpty()) {
                Toast.makeText(getApplicationContext(), "请将内容填写完成!", Toast.LENGTH_SHORT).show();
            } else {
                binding.btnEdit.setEnabled(false);
                Map<String, Object> map = new HashMap<>();
                map.put("userAccount", Objects.requireNonNull(GetUserInfo()).getUserAccount());
                map.put("userNickName", binding.etNickName.getText().toString());
                OKJsonPost(EditProfileActivity.this, new Gson().toJson(map), getString(R.string.update_profile), new OkSuccessInterface() {
                    @Override
                    public void OnSuccess(String json) {
                        Map<String, Object> map = new HashMap<>();
                        map = new Gson().fromJson(json, map.getClass());
                        Toast.makeText(getApplicationContext(), (String) map.get("status"), Toast.LENGTH_SHORT).show();
                    }
                });
                binding.btnEdit.setEnabled(true);
            }
        });

        /**
         * 返回点击事件
         */
        binding.back.setOnClickListener(v -> {
            finish();
        });

        /**
         * 动画
         */
//        binding.icBack.setOnClickListener(v -> {
//            方法一：
//            Animation rotateAnimation = AnimationUtils.loadAnimation(this, R.anim.xpage_zoom_in);
//            binding.icBack.startAnimation(rotateAnimation);
//            rotateAnimation.setFillAfter(true);


//            方法二：
//            Animator objectAnimation = AnimatorInflater.loadAnimator(this, R.animator.anim_translation);
//            objectAnimation.setTarget(binding.icBack);
//            objectAnimation.start();

//            方法三：
//            TranslateAnimation translateAnimation = new TranslateAnimation(binding.icBack.getPivotX(), 10.0f, 0.0f, 0.0f);
//            translateAnimation.setInterpolator(new LinearInterpolator());
//            translateAnimation.setDuration(500);
//            translateAnimation.setFillAfter(true);
//            binding.icBack.startAnimation(translateAnimation);
//        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }
}