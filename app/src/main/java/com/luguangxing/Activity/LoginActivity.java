package com.luguangxing.Activity;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.*;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.google.gson.Gson;
import com.kongzue.dialogx.DialogX;
import com.kongzue.dialogx.dialogs.MessageDialog;
import com.kongzue.dialogx.dialogs.PopTip;
import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.dialogs.WaitDialog;
import com.kongzue.dialogx.interfaces.DialogLifecycleCallback;
import com.kongzue.dialogx.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialogx.style.IOSStyle;
import com.kongzue.dialogx.style.MaterialStyle;
import com.kongzue.dialogx.util.TextInfo;
import com.luguangxing.Activity.databinding.ActivityLoginBinding;
import com.luguangxing.Bean.Login;
import com.luguangxing.Util.CommonUtils;
import com.luguangxing.Interface.OkSuccessInterface;
import com.xuexiang.xui.utils.StatusBarUtils;
import com.xuexiang.xui.utils.XToastUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.luguangxing.Util.CommonUtils.*;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        //设置沉浸式状态栏
        StatusBarUtils.translucent(this);
        //设置黑色字体状态栏
        StatusBarUtils.setStatusBarLightMode(this);
        setContentView(binding.getRoot());
        CommonUtils.init(getApplication());

        //初始化
        DialogX.init(this);
        //开启调试模式，在部分情况下会使用 Log 输出日志信息
        DialogX.DEBUGMODE = true;
        //设置为IOS主题
        DialogX.globalStyle = new IOSStyle();
        //设置亮色/暗色（在启动下一个对话框时生效）
        DialogX.globalTheme = DialogX.THEME.LIGHT;
        //设置对话框最大宽度（单位为像素）
        DialogX.dialogMaxWidth = 1920;
        //设置 InputDialog 自动弹出键盘
        DialogX.autoShowInputKeyboard = true;

        /**
         * 是否是从注销登录进来的
         */
        String userAccount = null;
        try {
            if (CommonUtils.GetAppData().equals("false")) {
                userAccount = CommonUtils.GetUserInfo().getUserAccount();
                binding.etUserAccount.setText(userAccount);
                if (userAccount != null) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            } else if (CommonUtils.GetAppData().equals("true")) {
                userAccount = CommonUtils.GetUserInfo().getUserAccount();
                binding.etUserAccount.setText(userAccount);
                binding.etUserPassword.setText(CommonUtils.GetUserInfo().getUserPassword());
            }
        } catch (Exception e) {
            XToastUtils.toast("您还没有登录！");
        }

        /**
         * 登 录
         */
        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.etUserAccount.getText().toString().isEmpty()
                        || binding.etUserPassword.getText().toString().isEmpty()) {
                    XToastUtils.info("账号或密码为空！");
                } else {
                    binding.btnLogin.setText("正在登录...");
                    binding.btnLogin.setEnabled(false);
                    Map<String, Object> map = new HashMap<>();
                    map.put("userAccount", binding.etUserAccount.getText().toString());
                    map.put("userPassword", binding.etUserPassword.getText().toString());
                    OKJsonPost(LoginActivity.this, new Gson().toJson(map), getString(R.string.login), new OkSuccessInterface() {
                        @Override
                        public void OnSuccess(String json) {
                            Login loginBean = new Gson().fromJson(json, Login.class);
                            if (loginBean.getStatus().equals("success")) {
                                TipDialog.show("登录成功", WaitDialog.TYPE.SUCCESS, 500).setDialogLifecycleCallback(new DialogLifecycleCallback<WaitDialog>() {
                                    @Override
                                    public void onDismiss(WaitDialog dialog) {
                                        super.onDismiss(dialog);
                                        SaveKey("UserInfo", "info", json);
                                        SaveKey("UserInfo", "isFromLogout", "false");
                                        SaveKey("UserInfo", "userCardId", loginBean.getData().getUserCardId());
                                        SaveKey("UserInfo", "token", loginBean.getToken());
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                            } else if (loginBean.getStatus().equals("userNoExist")) {
                                TipDialog.show("用户不存在！", WaitDialog.TYPE.ERROR);
                            } else if (loginBean.getStatus().equals("userPasswordError")) {
                                MessageDialog.show("密码错误", "请输入正确的密码！😊", "确定")
                                        .setOkTextInfo(new TextInfo().setFontColor(getResources().getColor(R.color.dialogxIOSBlue)))
                                        //.setTitleIcon(R.drawable.ic_incorrect)
                                        .setOkButton(new OnDialogButtonClickListener<MessageDialog>() {
                                            @Override
                                            public boolean onClick(MessageDialog baseDialog, View v) {
                                                binding.etUserPassword.requestFocus();
                                                return false;
                                            }
                                        });
                            }
                        }
                    });
                    binding.btnLogin.setEnabled(true);
                    binding.btnLogin.setText("登  录");
                }
            }
        });

        /**
         * 注 册
         */
        binding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        /**
         * 忘记密码
         */
        binding.btnForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);
                finish();
            }
        });

        /**
         * 验证码登录
         */
        binding.btnLoginByCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, LoginByCodeActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}