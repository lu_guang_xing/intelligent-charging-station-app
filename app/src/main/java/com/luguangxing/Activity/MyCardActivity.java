package com.luguangxing.Activity;

import static com.luguangxing.Util.CommonUtils.GetUserInfo;
import static com.luguangxing.Util.CommonUtils.OKJsonPost;
import static com.luguangxing.Util.CommonUtils.SaveKey;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.google.gson.Gson;
import com.kongzue.dialogx.dialogs.MessageDialog;
import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.dialogs.WaitDialog;
import com.kongzue.dialogx.interfaces.DialogLifecycleCallback;
import com.kongzue.dialogx.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialogx.util.TextInfo;
import com.luguangxing.Interface.OkSuccessInterface;
import com.luguangxing.Util.CommonUtils;
import com.xuexiang.xui.utils.StatusBarUtils;
import com.xuexiang.xui.utils.XToastUtils;
import com.xuexiang.xui.widget.imageview.RadiusImageView;
import com.xuexiang.xui.widget.popupwindow.ViewTooltip;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MyCardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置沉浸式状态栏
        StatusBarUtils.translucent(this);
        //设置黑色字体状态栏
        StatusBarUtils.setStatusBarLightMode(this);
        setContentView(R.layout.activity_my_card);

        RadiusImageView cardBack = findViewById(R.id.radius_image_view_card_back);
        TextView lostCard = findViewById(R.id.lost_card);
        TextView cardId = findViewById(R.id.card_id);
        TextView userName = findViewById(R.id.user_name_value);
        Button cardBinding = findViewById(R.id.btn_cancel_bind);
        if (CommonUtils.GetUserCardId() == null || CommonUtils.GetUserCardId().isEmpty()) {
            cardBinding.setVisibility(View.INVISIBLE);
            MessageDialog.show("未绑定", "您还没有绑定卡！😊", "立即绑定", "取消")
                    .setOkTextInfo(new TextInfo().setFontColor(getResources().getColor(R.color.dialogxIOSBlue)))
                    //.setTitleIcon(R.drawable.ic_incorrect)
                    .setOkButton(new OnDialogButtonClickListener<MessageDialog>() {
                        @Override
                        public boolean onClick(MessageDialog baseDialog, View v) {
                            Intent intent = new Intent(MyCardActivity.this, CardBinding.class);
                            startActivity(intent);
                            onPause();
                            if (CommonUtils.GetUserCardId() == null || CommonUtils.GetUserCardId().isEmpty()) {
                                XToastUtils.toast("绑定失败");
                            } else {
                                XToastUtils.toast("绑定成功");
                                cardBinding.setVisibility(View.VISIBLE);
                            }
                            return false;
                        }
                    })
                    .setCancelButton(new OnDialogButtonClickListener<MessageDialog>() {
                        @Override
                        public boolean onClick(MessageDialog baseDialog, View v) {
                            cardBinding.setVisibility(View.INVISIBLE);
                            return false;
                        }
                    });
        }
        cardId.setText(CommonUtils.GetUserCardId());
        userName.setText(CommonUtils.GetUserInfo().getUserName());

        cardBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewTooltip
                        .on(cardBack)
                        .position(ViewTooltip.Position.TOP)
                        .text("卡的背面")
                        .show();
            }
        });

        lostCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewTooltip
                        .on(lostCard)
                        .animation(new ViewTooltip.FadeTooltipAnimation())
                        .position(ViewTooltip.Position.BOTTOM)
                        .text("该功能还在开发中!来块🍕赞助一下？")
                        .show();
            }
        });

        cardBinding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDialog.show("提示", "确认取消绑定吗！😊", "确认", "取消")
                        .setOkTextInfo(new TextInfo().setFontColor(getResources().getColor(R.color.dialogxIOSBlue)))
                        //.setTitleIcon(R.drawable.ic_incorrect)
                        .setOkButton(new OnDialogButtonClickListener<MessageDialog>() {
                            @Override
                            public boolean onClick(MessageDialog baseDialog, View v) {
                                Map<String, Object> map = new HashMap<>();
                                map.put("condition", "userAccount");
                                map.put("conditionValue", Objects.requireNonNull(GetUserInfo()).getUserAccount().toString());
                                map.put("updateWhat", "userCardId");
                                map.put("updateValue", "");
                                SaveKey("UserInfo", "userCardId", map.get("updateValue").toString());
                                OKJsonPost(MyCardActivity.this, new Gson().toJson(map), getString(R.string.update_profile), new OkSuccessInterface() {
                                    @Override
                                    public void OnSuccess(String json) {
                                        Map<String, Object> map = new HashMap<>();
                                        map = new Gson().fromJson(json, map.getClass());
                                        if (map.get("status").equals("success")) {
                                            TipDialog.show("操作成功", WaitDialog.TYPE.SUCCESS, 500).setDialogLifecycleCallback(new DialogLifecycleCallback<WaitDialog>() {
                                                @Override
                                                public void onDismiss(WaitDialog dialog) {
                                                    MessageDialog.show("结果", "操作成功😊", "好的")
                                                            .setOkTextInfo(new TextInfo().setFontColor(getResources().getColor(R.color.dialogxIOSBlue)))
                                                            //.setTitleIcon(R.drawable.ic_incorrect)
                                                            .setOkButton(new OnDialogButtonClickListener<MessageDialog>() {
                                                                @Override
                                                                public boolean onClick(MessageDialog baseDialog, View v) {
                                                                    finish();
                                                                    return false;
                                                                }
                                                            });
                                                }
                                            });
                                        } else if (map.get("status").equals("error")) {
                                            TipDialog.show("操作失败", WaitDialog.TYPE.SUCCESS, 500).setDialogLifecycleCallback(new DialogLifecycleCallback<WaitDialog>() {
                                                @Override
                                                public void onDismiss(WaitDialog dialog) {

                                                }
                                            });
                                        }
                                    }
                                });
                                return false;
                            }
                        })
                        .setCancelButton(new OnDialogButtonClickListener<MessageDialog>() {
                            @Override
                            public boolean onClick(MessageDialog baseDialog, View v) {
                                return false;
                            }
                        });
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}