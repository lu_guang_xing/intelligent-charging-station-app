package com.luguangxing.Activity;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.gson.Gson;
import com.luguangxing.Activity.databinding.ActivityForgetPasswordBinding;
import com.luguangxing.Interface.OkSuccessInterface;
import com.luguangxing.Util.CommonUtils;
import com.xuexiang.xui.utils.CountDownButtonHelper;
import com.xuexiang.xui.utils.StatusBarUtils;
import com.xuexiang.xui.utils.XToastUtils;

import java.util.HashMap;
import java.util.Map;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.luguangxing.Util.CommonUtils.OKJsonPost;

public class ForgetPasswordActivity extends AppCompatActivity {
    private ActivityForgetPasswordBinding binding;
    private boolean isVerified;
    private String whatTextType;
    private boolean isPasswordVerify;
    private CountDownButtonHelper mCountDownHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        binding = ActivityForgetPasswordBinding.inflate(getLayoutInflater());
        //设置沉浸式状态栏
        StatusBarUtils.translucent(this);
        //设置黑色字体状态栏
        StatusBarUtils.setStatusBarLightMode(this);
        setContentView(binding.getRoot());
        isVerified = false;
        isPasswordVerify = false;
        mCountDownHelper = new CountDownButtonHelper(binding.btnGetVerifyCode, 60);

        /**
         * 返回密码登录
         */
        binding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        /**
         * 获取验证码
         */
        binding.btnGetVerifyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.etUserPhoneOrEmail.getText().toString().isEmpty()) {
                    XToastUtils.info("请填写手机号或邮箱！");
                } else {
                    mCountDownHelper.start();
                    binding.btnGetVerifyCode.setBackgroundColor(getResources().getColor(R.color.lightsteelblue));
                    if (CommonUtils.isEmail(binding.etUserPhoneOrEmail.getText().toString())) {
                        whatTextType = "邮箱";
                        Map<String, Object> map = new HashMap<>();
                        map.put("userEmail", binding.etUserPhoneOrEmail.getText().toString());
                        OKJsonPost(ForgetPasswordActivity.this, new Gson().toJson(map), getString(R.string.get_email_code), new OkSuccessInterface() {
                            @Override
                            public void OnSuccess(String json) {
                                Map<String, Object> map = new HashMap<>();
                                map = new Gson().fromJson(json, map.getClass());
                                if (map.get("status").equals("success")) {
                                    XToastUtils.success("验证码已发送到您的邮箱！");
                                }
                            }
                        });
                    } else if (CommonUtils.isTelPhoneNumber(binding.etUserPhoneOrEmail.getText().toString())) {
                        whatTextType = "手机号";
                        Map<String, Object> map = new HashMap<>();
                        map.put("userPhone", binding.etUserPhoneOrEmail.getText().toString());
                        OKJsonPost(ForgetPasswordActivity.this, new Gson().toJson(map), getString(R.string.get_phone_code), new OkSuccessInterface() {
                            @Override
                            public void OnSuccess(String json) {
                                Map<String, Object> map = new HashMap<>();
                                map = new Gson().fromJson(json, map.getClass());
                                if (map.get("status").equals("success")) {
                                    XToastUtils.success("验证码已发送到您的手机号！");
                                }
                            }
                        });
                    } else {
                        XToastUtils.info("您输入正确的手机号或邮箱！");
                    }
                }
            }
        });

        /**
         * 判断输入的验证码是否正确
         */
        binding.etVerifyCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.etVerifyCode.getText().length() == 4) {
                    binding.ivVerify.setVisibility(VISIBLE);
                    Map<String, Object> map = new HashMap<>();
                    map.put("phoneOrEmail", binding.etUserPhoneOrEmail.getText().toString());
                    map.put("code", binding.etVerifyCode.getText().toString());
                    OKJsonPost(ForgetPasswordActivity.this, new Gson().toJson(map), getString(R.string.verify_code), new OkSuccessInterface() {
                        @Override
                        public void OnSuccess(String json) {
                            Map<String, Object> map = new HashMap<>();
                            map = new Gson().fromJson(json, map.getClass());
                            if (map.get("status").equals("success")) {
                                binding.ivVerify.setImageResource(R.drawable.ic_correct);
                                isVerified = true;
                            } else {
                                isVerified = false;
                                binding.ivVerify.setImageResource(R.drawable.ic_issue);
                            }
                        }
                    });
                } else if (binding.etVerifyCode.getText().length() == 0) {
                    binding.ivVerify.setVisibility(INVISIBLE);
                    isVerified = false;
                } else if (binding.etVerifyCode.getText().length() > 4 ||
                        binding.etVerifyCode.getText().length() < 4) {
                    binding.ivVerify.setVisibility(VISIBLE);
                    isVerified = false;
                    binding.ivVerify.setImageResource(R.drawable.ic_issue);
                }
            }
        });

        /**
         * 判断两个密码是否一样
         */
        binding.etUserPasswordRe.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.etUserPasswordRe.getText().toString().equals(
                        binding.etUserPassword.getText().toString())) {
                    isPasswordVerify = true;
                    binding.ivPasswordVerify.setVisibility(VISIBLE);
                    binding.ivPasswordVerify.setImageResource(R.drawable.ic_correct);
                } else if (binding.etUserPasswordRe.getText().toString().isEmpty()) {
                    binding.ivPasswordVerify.setVisibility(INVISIBLE);
                    isPasswordVerify = false;
                } else if (!binding.etUserPasswordRe.getText().toString().isEmpty()) {
                    isPasswordVerify = false;
                    binding.ivPasswordVerify.setVisibility(VISIBLE);
                    binding.ivPasswordVerify.setImageResource(R.drawable.ic_issue);
                }
            }
        });

        /**
         * 确认重置密码
         */
        binding.btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isVerified) {
                    XToastUtils.info("验证码错误！");
                } else if (!isPasswordVerify) {
                    XToastUtils.info("两次密码不一样！");
                } else {
                    binding.btnResetPassword.setText("正在重置....");
                    binding.btnResetPassword.setEnabled(false);
                    Map<String, Object> map = new HashMap<>();
                    map.put("userPassword", binding.etUserPassword.getText().toString());
                    if (whatTextType.equals("邮箱")) {
                        map.put("condition", "userEmail");
                    } else if (whatTextType.equals("手机号")) {
                        map.put("condition", "userPhone");
                    }
                    map.put("conditionValue", binding.etUserPhoneOrEmail.getText().toString());
                    OKJsonPost(ForgetPasswordActivity.this, new Gson().toJson(map), getString(R.string.update_profile), new OkSuccessInterface() {
                        @Override
                        public void OnSuccess(String json) {
                            Map<String, Object> map = new HashMap<>();
                            map = new Gson().fromJson(json, map.getClass());
                            if (map.get("status").equals("success")) {
                                XToastUtils.success("密码已重置！");
                            } else if (map.get("status").equals("error")) {
                                XToastUtils.warning("重置失败！");
                            }
                        }
                    });
                    binding.btnResetPassword.setEnabled(true);
                    binding.btnResetPassword.setText("密码重置");
                }
            }
        });

        binding.etUserPhoneOrEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (CommonUtils.isEmail(binding.etUserPhoneOrEmail.getText().toString())) {
                    whatTextType = "邮箱";
                } else if (CommonUtils.isTelPhoneNumber(binding.etUserPhoneOrEmail.getText().toString())) {
                    whatTextType = "手机号";
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCountDownHelper.cancel();
    }

}