package com.luguangxing.Activity;

import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.gson.Gson;
import com.luguangxing.Activity.databinding.ActivityEditPasswordBinding;
import com.luguangxing.Interface.OkSuccessInterface;
import com.xuexiang.xui.utils.StatusBarUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.luguangxing.Util.CommonUtils.GetUserInfo;
import static com.luguangxing.Util.CommonUtils.OKJsonPost;

public class EditPasswordActivity extends AppCompatActivity {
    private ActivityEditPasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_edit_password);
        binding = ActivityEditPasswordBinding.inflate(getLayoutInflater());
        //设置沉浸式状态栏
        StatusBarUtils.translucent(this);
        //设置黑色字体状态栏
        StatusBarUtils.setStatusBarLightMode(this);
        setContentView(binding.getRoot());
        binding.btnEditPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.etUserPassword.getText().toString().isEmpty()
                        || binding.etUserPasswordRe.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "请将内容填写完成!", Toast.LENGTH_SHORT).show();
                } else if (!binding.etUserPassword.getText().toString().equals(
                        binding.etUserPasswordRe.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "两次密码不一致", Toast.LENGTH_SHORT).show();
                } else {
                    binding.btnEditPassword.setEnabled(false);
                    Map<String, Object> map = new HashMap<>();
                    map.put("userAccount", Objects.requireNonNull(GetUserInfo()).getUserAccount());
                    map.put("userPassword", binding.etUserPassword.getText().toString());
                    OKJsonPost(EditPasswordActivity.this, new Gson().toJson(map), getString(R.string.edit_password), new OkSuccessInterface() {
                        @Override
                        public void OnSuccess(String json) {
                            Map<String, Object> map = new HashMap<>();
                            map = new Gson().fromJson(json, map.getClass());
                            if (map.get("status").equals("修改成功")) {
                                Toast.makeText(getApplicationContext(), (String) map.get("status"), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    binding.btnEditPassword.setEnabled(true);
                }
            }
        });
    }
}