package com.luguangxing.Activity;

import android.content.Intent;
import android.os.Handler;

import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.google.gson.Gson;
import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.dialogs.WaitDialog;
import com.luguangxing.Activity.databinding.ActivityUseDeviceBinding;
import com.luguangxing.Interface.OkSuccessInterface;
import com.luguangxing.Util.CommonUtils;
import com.xuexiang.xui.utils.StatusBarUtils;
import com.xuexiang.xui.utils.XToastUtils;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ScheduledExecutorService;

import android.annotation.SuppressLint;
import android.os.Message;
import android.view.View;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.luguangxing.Util.CommonUtils.GetUserInfo;
import static com.luguangxing.Util.CommonUtils.OKJsonPost;

public class UseDeviceActivity extends AppCompatActivity {
    private ActivityUseDeviceBinding binding;
    Map<String, Object> map;
    private String host = "tcp://43.138.179.149:1883";
    private String userName = "admin";
    private String passWord = "whiteone@lu";
    private String mqtt_id = "mqttdemo11";
    private String mqtt_pub_topic = "ToDevice/1001";
    private String mqtt_sub_topic = "toAndroid/";
    private DecimalFormat format = new DecimalFormat("0.0 ");

    private Handler handler;
    private MqttClient client;
    private MqttConnectOptions options;
    private ScheduledExecutorService scheduler;

    private int chargeHour = 1;
    private int port = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityUseDeviceBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(binding.getRoot());
        Intent intent = getIntent();
        String deviceNum = intent.getStringExtra("deviceNum");//从intent中读取信息
        String interfaceNum = intent.getStringExtra("interfaceNum");//从intent中读取信息
        String deviceAddress = intent.getStringExtra("deviceAddress");//从intent中读取信息
        String deviceImei = intent.getStringExtra("deviceImei");//从intent中读取信息
        String pricePerHour = intent.getStringExtra("pricePerHour");//从intent中读取信息
        //设置沉浸式状态栏
        StatusBarUtils.translucent(this);
        //设置黑色字体状态栏
        StatusBarUtils.setStatusBarLightMode(this);
        map = new HashMap<>();
        binding.deviceNum.setText(binding.deviceNum.getText() + deviceNum);
        binding.interfaceNum.setText(binding.interfaceNum.getText() + interfaceNum);
        binding.price.setText(binding.price.getText() + pricePerHour + "元/小时");
        this.mqtt_sub_topic += deviceImei;

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.RadioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton r = (RadioButton) findViewById(checkedId);
                port = Integer.parseInt(r.getText().toString().replace("号", ""));
            }
        });

        binding.RadioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton r = (RadioButton) findViewById(checkedId);
                chargeHour = Integer.parseInt(r.getText().toString().replace("小时", ""));
            }
        });

        binding.charge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map.clear();
                map.put("port", port);
                map.put("chargeHour", chargeHour);
                map.put("userCardId", Objects.requireNonNull(GetUserInfo()).getUserCardId());
                publishMessage(mqtt_pub_topic, new Gson().toJson(map));
            }
        });

        init();
        startReconnect();
        handler = new Handler() {
            @SuppressLint({"SetTextIl8n", "HandlerLeak"})
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1: //开机校验更新回传
                        break;
                    case 2: //反馈回转
                        break;
                    case 3: //MQTT收到消息回传
                        map.clear();
                        map = new Gson().fromJson(msg.obj.toString(), map.getClass());

                        break;
                    case 30: //连接失败
                        XToastUtils.info("连接失败");
                        TipDialog.show("连接失败!", WaitDialog.TYPE.WARNING);
                        break;
                    case 31: //连接成功
                        try {
                            client.subscribe(mqtt_sub_topic, 0);//订阅
                        } catch (MqttException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        break;
                }
            }
        };
    }

    private void init() {
        try {
            //host为主机名，test为clientid即连接MQTT的客户端ID，一般以客户端唯一标识符表示，MemoryPersistence设置clientid的保存形式，默认为以内存保存
            client = new MqttClient(host, mqtt_id,
                    new MemoryPersistence());
            //MQTT的连接设置
            options = new MqttConnectOptions();
            //设置是否清空session,这里如果设置为false表示服务器会保留客户端的连接记录，这里设置为true表示每次连接到服务器都以新的身份连接
            options.setCleanSession(true);
            //设置连接的用户名
            options.setUserName(userName);
            //设置连接的密码
            options.setPassword(passWord.toCharArray());
            // 设置超时时间 单位为秒
            options.setConnectionTimeout(10);
            // 设置会话心跳时间 单位为秒 服务器会每隔1.5*20秒的时间向客户端发送个消息判断客户端是否在线，但这个方法并没有重连的机制
            options.setKeepAliveInterval(20);
            //设置回调
            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                    //连接丢失后，一般在这里面进行重连
                    System.out.println("connectionLost----------");
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    //publish后会执行到这里
                    System.out.println("deliveryComplete---------"
                            + token.isComplete());
                }

                @Override
                public void messageArrived(String topicName, MqttMessage message)
                        throws Exception {
                    //subscribe后得到的消息会执行到这里面
                    System.out.println("messageArrived----------");
                    Message msg = new Message();
                    msg.what = 3;
                    msg.obj = message.toString();
                    handler.sendMessage(msg);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Mqtt_connect() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (!(client.isConnected())) {
                        client.connect(options);
                        Message msg = new Message();
                        msg.what = 31;
                        handler.sendMessage(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Message msg = new Message();
                    msg.what = 30;
                    handler.sendMessage(msg);
                }
            }
        }).start();
    }

    private void startReconnect() {
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (!client.isConnected()) {
                    Mqtt_connect();
                }
            }
        }, 0 * 1000, 10 * 1000, TimeUnit.MILLISECONDS);
    }

    private void publishMessage(String topic, String message2) {
        if (client == null || !client.isConnected()) {
            return;
        }
        MqttMessage message = new MqttMessage();
        message.setPayload(message2.getBytes());
        try {
            client.publish(topic, message);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        client = null;
        handler = null;
        super.onDestroy();
        if (map != null) {
            map.clear();
            map = null;
        }
        if (binding != null) {
            binding = null;
        }
    }
}