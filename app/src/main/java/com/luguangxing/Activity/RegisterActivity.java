package com.luguangxing.Activity;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.google.gson.Gson;
import com.kongzue.dialogx.dialogs.MessageDialog;
import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.dialogs.WaitDialog;
import com.kongzue.dialogx.interfaces.BaseDialog;
import com.kongzue.dialogx.interfaces.DialogLifecycleCallback;
import com.kongzue.dialogx.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialogx.util.TextInfo;
import com.luguangxing.Activity.databinding.ActivityRegisterBinding;
import com.luguangxing.Interface.OkSuccessInterface;
import com.luguangxing.Util.CommonUtils;
import com.xuexiang.xui.utils.CountDownButtonHelper;
import com.xuexiang.xui.utils.StatusBarUtils;
import com.xuexiang.xui.utils.XToastUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.luguangxing.Util.CommonUtils.GetUserInfo;
import static com.luguangxing.Util.CommonUtils.OKJsonPost;
import static com.luguangxing.Util.CommonUtils.SaveKey;

public class RegisterActivity extends AppCompatActivity {
    private ActivityRegisterBinding binding;
    private boolean isVerified;
    private boolean isRegister;
    private CountDownButtonHelper mCountDownHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        //设置沉浸式状态栏
        StatusBarUtils.translucent(this);
        //设置黑色字体状态栏
        StatusBarUtils.setStatusBarLightMode(this);
        setContentView(binding.getRoot());
        isVerified = false;
        isRegister = false;
        mCountDownHelper = new CountDownButtonHelper(binding.btnGetVerifyCode, 60);

        /**
         * 确认注册
         */
        binding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.etUserAccount.getText().toString().isEmpty()
                        || binding.etNickName.getText().toString().isEmpty()
                        || binding.etUserPassword.getText().toString().isEmpty()
                        || binding.etUserPasswordRe.getText().toString().isEmpty()
                        || binding.etUserPhoneOrEmail.getText().toString().isEmpty()) {
                    TipDialog.show("请将内容填写完整！", WaitDialog.TYPE.WARNING);
                } else if (!binding.etUserPassword.getText().toString().equals(
                        binding.etUserPasswordRe.getText().toString())) {
                    TipDialog.show("两次密码不一致！", WaitDialog.TYPE.WARNING);
                } else {
                    XToastUtils.info("正在注册");
                    Map<String, Object> map = new HashMap<>();
                    map.put("userAccount", binding.etUserAccount.getText().toString());
                    map.put("userName", binding.etNickName.getText().toString());
                    map.put("userPassword", binding.etUserPassword.getText().toString());
                    if (binding.etUserPhoneOrEmail.getHint().toString().equals("手机号")) {
                        map.put("userPhone", binding.etUserPhoneOrEmail.getText().toString());
                    } else if (binding.etUserPhoneOrEmail.getHint().toString().equals("邮箱")) {
                        map.put("userEmail", binding.etUserPhoneOrEmail.getText().toString());
                    }
                    OKJsonPost(RegisterActivity.this, new Gson().toJson(map), getString(R.string.register), new OkSuccessInterface() {
                        @Override
                        public void OnSuccess(String json) {
                            Map<String, Object> map = new HashMap<>();
                            map = new Gson().fromJson(json, map.getClass());
                            if (map.get("status").equals("success")) {
                                MessageDialog.show("注册成功！", "\n", "好的")
                                        .setOkTextInfo(new TextInfo().setFontColor(getResources().getColor(R.color.dialogxIOSBlue)));
                            } else {
                                binding.btnRegister.setEnabled(true);
                                MessageDialog.show("注册失败", "\n", "好的")
                                        .setOkTextInfo(new TextInfo().setFontColor(getResources().getColor(R.color.dialogxIOSBlue)))
                                        .setOkButton(new OnDialogButtonClickListener() {
                                            @Override
                                            public boolean onClick(BaseDialog baseDialog, View v) {
                                                return false;
                                            }
                                        });
                                TipDialog.show("注册失败！", WaitDialog.TYPE.ERROR);
                            }
                        }
                    });
                }
            }
        });

        /**
         * 返回密码登录
         */
        binding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        /**
         * 获取验证码
         */
        binding.btnGetVerifyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.etUserPhoneOrEmail.getText().toString().isEmpty()) {
                    TipDialog.show("请填写" + binding.etUserPhoneOrEmail.getHint().toString() + "！", WaitDialog.TYPE.WARNING);
                } else {
                    mCountDownHelper.start();
                    binding.btnGetVerifyCode.setBackgroundColor(getResources().getColor(R.color.lightsteelblue));
                    Map<String, Object> map = new HashMap<>();
                    if (!isRegister) {
                        if (binding.etUserPhoneOrEmail.getHint().toString().equals("邮箱地址")) {
                            if (CommonUtils.isEmail(binding.etUserPhoneOrEmail.getText().toString())) {
                                map.put("userEmail", binding.etUserPhoneOrEmail.getText().toString());
                                OKJsonPost(RegisterActivity.this, new Gson().toJson(map), getString(R.string.get_register_email_code), new OkSuccessInterface() {
                                    @Override
                                    public void OnSuccess(String json) {
                                        Map<String, Object> map = new HashMap<>();
                                        map = new Gson().fromJson(json, map.getClass());
                                        if (map.get("status").equals("success")) {
                                            TipDialog.show("验证码已发送到您的邮箱！", WaitDialog.TYPE.SUCCESS);
                                        } else if (map.get("status").equals("userExist")) {
                                            TipDialog.show("该邮箱已注册！", WaitDialog.TYPE.WARNING);
                                            mCountDownHelper.cancel();
                                            binding.btnGetVerifyCode.setEnabled(true);
                                        }
                                    }
                                });
                            } else {
                                TipDialog.show("您输入的邮箱不合法！", WaitDialog.TYPE.WARNING);
                                mCountDownHelper.cancel();
                                binding.btnGetVerifyCode.setEnabled(true);
                            }
                        } else if (binding.etUserPhoneOrEmail.getHint().toString().equals("手机号")) {
                            if (CommonUtils.isTelPhoneNumber(binding.etUserPhoneOrEmail.getText().toString())) {
                                map.put("userPhone", binding.etUserPhoneOrEmail.getText().toString());
                                OKJsonPost(RegisterActivity.this, new Gson().toJson(map), getString(R.string.get_register_phone_code), new OkSuccessInterface() {
                                    @Override
                                    public void OnSuccess(String json) {
                                        Map<String, Object> map = new HashMap<>();
                                        map = new Gson().fromJson(json, map.getClass());
                                        if (map.get("status").equals("success")) {
                                            TipDialog.show("验证码已发送到您的手机号！", WaitDialog.TYPE.SUCCESS);
                                        } else if (map.get("status").equals("userExist")) {
                                            TipDialog.show("该手机号已注册！", WaitDialog.TYPE.WARNING);
                                            mCountDownHelper.cancel();
                                            binding.btnGetVerifyCode.setEnabled(true);
                                        }
                                    }
                                });
                            } else {
                                TipDialog.show("您输入的手机号不合法！", WaitDialog.TYPE.WARNING);
                                mCountDownHelper.cancel();
                                binding.btnGetVerifyCode.setEnabled(true);
                            }
                        }
                    }
                }
            }
        });

        /**
         * 手机号注册
         */
        binding.btnRegisterByPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MessageDialog.show("提示", "\n服务已停止,暂时不可用😭\n", "好的")
                        .setOkTextInfo(new TextInfo().setFontColor(getResources().getColor(R.color.dialogxIOSBlue)))
                        //.setTitleIcon(R.drawable.ic_incorrect)
                        .setOkButton(new OnDialogButtonClickListener<MessageDialog>() {
                            @Override
                            public boolean onClick(MessageDialog baseDialog, View v) {

                                return false;
                            }
                        });
//                binding.etUserPhoneOrEmail.setHint("手机号");
//                binding.btnRegisterByPhone.setTextColor(getResources().getColor(R.color.black));
//                binding.btnRegisterByEmail.setTextColor(getResources().getColor(R.color.grey));
            }
        });

        /**
         * 邮箱注册
         */
        binding.btnRegisterByEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.etUserPhoneOrEmail.setHint("邮箱地址");
                binding.btnRegisterByEmail.setTextColor(getResources().getColor(R.color.black));
                binding.btnRegisterByPhone.setTextColor(getResources().getColor(R.color.grey));
            }
        });

        /**
         * 判断输入的验证码是否正确
         */
        binding.etVerifyCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (binding.etVerifyCode.getText().length() == 4) {
                    binding.ivVerify.setVisibility(VISIBLE);
                    Map<String, Object> map = new HashMap<>();
                    map.put("phoneOrEmail", binding.etUserPhoneOrEmail.getText().toString());
                    map.put("code", binding.etVerifyCode.getText().toString());
                    OKJsonPost(RegisterActivity.this, new Gson().toJson(map), getString(R.string.verify_code), new OkSuccessInterface() {
                        @Override
                        public void OnSuccess(String json) {
                            Map<String, Object> map = new HashMap<>();
                            map = new Gson().fromJson(json, map.getClass());
                            if (map.get("status").equals("success")) {
                                binding.ivVerify.setImageResource(R.drawable.ic_correct);
                                isVerified = true;
                            } else {
                                isVerified = false;
                                binding.ivVerify.setImageResource(R.drawable.ic_issue);
                            }
                        }
                    });
                } else if (binding.etVerifyCode.getText().length() == 0) {
                    binding.ivVerify.setVisibility(INVISIBLE);
                    isVerified = false;
                } else if (binding.etVerifyCode.getText().length() > 4 ||
                        binding.etVerifyCode.getText().length() < 4) {
                    binding.ivVerify.setVisibility(VISIBLE);
                    isVerified = false;
                    binding.ivVerify.setImageResource(R.drawable.ic_issue);
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCountDownHelper.cancel();
    }
}