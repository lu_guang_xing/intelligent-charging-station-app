package com.luguangxing.service;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

/**
 * @author: 小白一号
 * @date: 2022/11/28
 * @desc:
 */
public class MQTTServiceConnection implements ServiceConnection {

    private MQTTService mqttService;

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        MQTTService.CustomBinder binder = (MQTTService.CustomBinder) service;
        mqttService = binder.getService();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    public MQTTService getMqttService(){
        if(mqttService != null){
            return mqttService;
        }
        return null;
    }
}
