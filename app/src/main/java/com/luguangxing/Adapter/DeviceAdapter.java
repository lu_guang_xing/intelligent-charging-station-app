package com.luguangxing.Adapter;

import static com.xuexiang.xutil.XUtil.getResources;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.kongzue.dialogx.dialogs.MessageDialog;
import com.kongzue.dialogx.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialogx.util.TextInfo;
import com.luguangxing.Activity.CardBinding;
import com.luguangxing.Activity.MyCardActivity;
import com.luguangxing.Activity.R;
import com.luguangxing.Activity.UseDeviceActivity;
import com.luguangxing.Bean.StationBean;
import com.luguangxing.Util.CommonUtils;
import com.xuexiang.xui.utils.XToastUtils;

import java.util.List;

public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.MyViewHolder> {
    List<StationBean> stations;
    private Context mContext;

    public DeviceAdapter(Context context, List<StationBean> stations) {
        mContext = context;
        this.stations = stations;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_item_home, parent, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.deviceNum.setText("" + stations.get(position).getDeviceNum() + "号充电桩");
        holder.interfaceNum.setText("" + stations.get(position).getInterfaceNum() + " 空闲");
        holder.deviceAddress.setText("" + stations.get(position).getDeviceAddress());
        holder.status.setText("" + stations.get(position).getDeviceStatus());
        //holder.icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_sds_device));
        if (holder.status.getText().toString().equals("离线")) {
            holder.statusIcon.setImageResource(R.color.color_gray_6);
            holder.iconDevice.setImageResource(R.drawable.icon_charger_station_2);
            holder.item.setImageResource(R.color.color_gray_8);
            holder.interfaceNum.setText("不可用");
        } else if (holder.status.getText().toString().equals("异常")) {
            holder.statusIcon.setImageResource(R.color.orange);
        }
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.status.getText().toString().equals("离线")) {
                    MessageDialog.show("ChargerOX", "\n该充电桩处于离线状态！😊\n", "好的")
                            .setOkTextInfo(new TextInfo().setFontColor(getResources().getColor(R.color.dialogxIOSBlue)));
                } else {
                    Intent intent = new Intent(mContext, UseDeviceActivity.class);
                    intent.putExtra("deviceNum", "" + stations.get(position).getDeviceNum());
                    intent.putExtra("interfaceNum", "" + stations.get(position).getInterfaceNum());
                    intent.putExtra("deviceAddress", "" + stations.get(position).getDeviceAddress());
                    intent.putExtra("deviceStatus", "" + stations.get(position).getDeviceStatus());
                    intent.putExtra("deviceImei", "" + stations.get(position).getDeviceImei());
                    intent.putExtra("pricePerHour", "" + stations.get(position).getPricePerHour());
                    mContext.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView deviceNum;
        TextView interfaceNum;
        TextView deviceAddress;
        TextView status;
        com.xuexiang.xui.widget.imageview.RadiusImageView iconDevice;
        com.xuexiang.xui.widget.imageview.RadiusImageView item;
        com.xuexiang.xui.widget.imageview.RadiusImageView statusIcon;

        public MyViewHolder(View view) {
            super(view);
            deviceNum = (TextView) view.findViewById(R.id.device_num);
            interfaceNum = (TextView) view.findViewById(R.id.interface_num);
            deviceAddress = (TextView) view.findViewById(R.id.device_address);
            status = (TextView) view.findViewById(R.id.status_value);
            iconDevice = (com.xuexiang.xui.widget.imageview.RadiusImageView) view.findViewById(R.id.icon_device);
            item = (com.xuexiang.xui.widget.imageview.RadiusImageView) view.findViewById(R.id.round_background);
            statusIcon = (com.xuexiang.xui.widget.imageview.RadiusImageView) view.findViewById(R.id.ic_status);
        }
    }
}