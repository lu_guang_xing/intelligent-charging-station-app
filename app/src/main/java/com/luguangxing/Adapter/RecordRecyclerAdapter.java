package com.luguangxing.Adapter;

import android.view.View;

import androidx.annotation.NonNull;

import com.luguangxing.Activity.R;
import com.luguangxing.Bean.CostRecord;
import com.xuexiang.xui.adapter.recyclerview.BaseRecyclerAdapter;
import com.xuexiang.xui.adapter.recyclerview.RecyclerViewHolder;
import com.xuexiang.xui.utils.XToastUtils;

import java.util.Collection;

public class RecordRecyclerAdapter extends BaseRecyclerAdapter<CostRecord> implements RecyclerViewHolder.OnItemClickListener {
    //private RecyclerViewHolder.OnItemClickListener myClickListener;
    public RecordRecyclerAdapter() {

    }

    public RecordRecyclerAdapter(Collection<CostRecord> list) {
        super(list);
    }

    @Override
    protected void bindData(@NonNull RecyclerViewHolder holder, int position, CostRecord item) {
//        holder.text(R.id.order_id_value, item.getOrderId());
//        holder.text(R.id.date_value, item.getDate());
//        holder.text(R.id.payload_value, "-" + item.getCost() + "元");
//        holder.click(R.id.order_id_value, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                XToastUtils.info("你点击了" + position);
//            }
//        });
    }

    @Override
    protected int getItemLayoutId(int viewType) {
        return R.layout.recycler_item_message;
    }

    @Override
    public void onItemClick(View itemView, Object item, int position) {
        XToastUtils.info("你点击了" + position);
    }
}
