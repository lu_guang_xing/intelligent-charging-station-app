//package com.luguangxing.Adapter;
//
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.luguangxing.Activity.R;
//import com.luguangxing.Util.CommonUtils;
//
//import java.util.List;
//import java.util.Map;
//
//public class CommandResponseAdapter extends RecyclerView.Adapter<CommandResponseAdapter.MyViewHolder> {
//    List<Map<String, Object>> uploadedData;
//    private Context mContext;
//
//    public CommandResponseAdapter(Context context, List<Map<String, Object>> uploadData) {
//        mContext = context;
//        this.uploadedData = uploadData;
//    }
//
//    @Override
//    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_item_interface, parent, false));
//        return holder;
//    }
//
//    @Override
//    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
//        holder.commandId.setText("指令ID:" + uploadedData.get(position).get("commandId").toString());
//        holder.date.setText("日期:" + CommonUtils.toDate(Long.parseLong(uploadedData.get(position).get("finishTime").toString())));
//        holder.command.setText("指令:" + uploadedData.get(position).get("command").toString());
//        holder.commandStatus.setText(uploadedData.get(position).get("commandStatus").toString());
//    }
//
//    @Override
//    public int getItemCount() {
//        return uploadedData.size();
//    }
//
//    class MyViewHolder extends RecyclerView.ViewHolder {
//        TextView commandId;
//        TextView date;
//        TextView command;
//        TextView commandStatus;
//
//        public MyViewHolder(View view) {
//            super(view);
//            commandId = (TextView) view.findViewById(R.id.command_id);
//            date = (TextView) view.findViewById(R.id.date);
//            command = (TextView) view.findViewById(R.id.command);
//            commandStatus = (TextView) view.findViewById(R.id.command_status);
//        }
//    }
//}