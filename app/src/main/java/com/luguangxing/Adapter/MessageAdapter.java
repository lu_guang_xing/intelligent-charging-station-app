package com.luguangxing.Adapter;

import static com.xuexiang.xutil.XUtil.getResources;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.luguangxing.Activity.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {
    List<Map<String, Object>> uploadedData;
    private Context mContext;

    public MessageAdapter(Context context, List<Map<String, Object>> uploadData) {
        mContext = context;
        this.uploadedData = uploadData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder holder = new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.recycler_item_message, parent, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Map<String, Object> map = new HashMap<>();
        String payload = uploadedData.get(position).get("payload").toString();
        map = new Gson().fromJson(payload, map.getClass());
        payload = payload.substring(payload.indexOf("\"") + 1, payload.lastIndexOf("\""));
        holder.payload.setText("" + payload);
        holder.date.setText("日期:" + uploadedData.get(position).get("timestamp"));
        holder.protocol.setText("传输方式:" + uploadedData.get(position).get("protocol").toString().toUpperCase());
//        if (payload.equals("Temperature_Data")) {
//            holder.payLoadValue.setText("" + map.get(payload) + " ℃");
//            holder.item.setImageDrawable(getResources().getDrawable(R.drawable.ic_tempratrue));
//        } else if (payload.equals("Light_Data")) {
//            holder.payLoadValue.setText("" + map.get(payload) + " Lx");
//            holder.item.setImageDrawable(getResources().getDrawable(R.drawable.ic_light_device));
//        }
    }

    @Override
    public int getItemCount() {
        return uploadedData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView payload;
        TextView date;
        TextView protocol;
        TextView payLoadValue;
        com.xuexiang.xui.widget.imageview.RadiusImageView item;

        public MyViewHolder(View view) {
            super(view);
            payload = (TextView) view.findViewById(R.id.payload);
            date = (TextView) view.findViewById(R.id.date);
            protocol = (TextView) view.findViewById(R.id.protocol);
//            payLoadValue = (TextView) view.findViewById(R.id.payload_value);
            item = (com.xuexiang.xui.widget.imageview.RadiusImageView) view.findViewById(R.id.icon_device);
        }
    }
}