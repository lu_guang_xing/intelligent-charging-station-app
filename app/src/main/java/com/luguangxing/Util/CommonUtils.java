package com.luguangxing.Util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kongzue.dialogx.dialogs.MessageDialog;
import com.kongzue.dialogx.dialogs.TipDialog;
import com.kongzue.dialogx.dialogs.WaitDialog;
import com.kongzue.dialogx.interfaces.DialogLifecycleCallback;
import com.kongzue.dialogx.interfaces.OnDialogButtonClickListener;
import com.kongzue.dialogx.util.TextInfo;
import com.luguangxing.Activity.LoginActivity;
import com.luguangxing.Activity.MainActivity;
import com.luguangxing.Activity.R;
import com.luguangxing.Bean.RecordStatus;
import com.luguangxing.Bean.Login;
import com.luguangxing.Bean.User;
import com.luguangxing.Interface.OkSuccessInterface;

import com.xuexiang.xutil.app.ActivityUtils;
import okhttp3.*;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.kongzue.dialogx.DialogX.DEBUGMODE;
import static com.xuexiang.xutil.XUtil.getPackageManager;
import static com.xuexiang.xutil.app.ActivityUtils.startActivity;
import static com.xuexiang.xutil.resource.ResUtils.getResources;
import static com.xuexiang.xutil.resource.ResUtils.getString;

public class CommonUtils {
    public static Application context = new Application();//上下文
    public static List<RecordStatus> groupByMonthList;

    /**
     * 获取上下文
     */
    public static void init(Application application) {
        context = application;
    }

    //流转换为String
    private String read(InputStream in) {
        BufferedReader reader = null;
        StringBuilder sb = null;
        String line = null;
        try {
            sb = new StringBuilder();//实例化一个StringBuilder对象
            //用InputStreamReader把in这个字节流转换成字符流BufferedReader
            reader = new BufferedReader(new InputStreamReader(in));
            //判断从reader中读取的行内容是否为空
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } finally {
            try {
                if (in != null) in.close();
                if (reader != null) reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    //给ImageView控件设置网络图片
    public static void setImageByUrl(ImageView imageView, String url) {
        Handler imageHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    Bitmap bm = (android.graphics.Bitmap) msg.obj;
                    imageView.setImageBitmap(bm);
                } else if (msg.what == 0) {
                    Log.i("Error", "can't get the image resources!");
                }
            }
        };
        new Thread() {
            private HttpURLConnection conn;
            private Bitmap bitmap;

            @Override
            public void run() {
                try {
                    URL urls = new URL(url);
                    conn = (HttpURLConnection) urls.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setConnectTimeout(5000);
                    int code = conn.getResponseCode();
                    if (code == 200) {
                        InputStream is = conn.getInputStream();
                        bitmap = BitmapFactory.decodeStream(is);
                        Message msg = new Message();
                        msg.what = 1;
                        msg.obj = bitmap;
                        imageHandler.sendMessage(msg);
                    } else {
                        Message msg = new Message();
                        msg.what = 0;
                        imageHandler.sendMessage(msg);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /**
     * OKhttp封装(json参数传递方式)无加载动画
     */
    public static void OKJsonPost(Activity activity, String json, String url, OkSuccessInterface successInterface) {
        //创建OkHttpClient请求对象
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)//设置连接超时时间
                .readTimeout(60, TimeUnit.SECONDS)//设置读取超时时间
                .build();
        //创建一个RequestBody(参数1：数据类型 参数2传递的json串)

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), json);

        String token = "A";
        try {
            if (GetToken() != null)
                token = GetToken();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //创建一个请求对象
        Request request = new Request.Builder()
                .url(url)
                .addHeader("token", token)
                .post(requestBody)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @SuppressLint("CheckResult")
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                assert activity != null;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity.getApplicationContext(), "服务器无响应", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (successInterface != null) {
                    String json = Objects.requireNonNull(response.body()).string();
                    assert activity != null;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (json.contains("tokenExpired")) {
                                Map<String, Object> map = new HashMap<>();
                                map.put("userAccount", GetUserInfo().getUserAccount());
                                map.put("userPassword", GetUserInfo().getUserPassword());
                                OKJsonPost(activity, new Gson().toJson(map), getString(R.string.login), new OkSuccessInterface() {
                                    @Override
                                    public void OnSuccess(String json) {
                                        Login loginBean = new Gson().fromJson(json, Login.class);
                                        if (loginBean.getStatus().equals("success")) {
                                            SaveKey("UserInfo", "info", json);
                                            SaveKey("UserInfo", "isFromLogout", "false");
                                            SaveKey("UserInfo", "userCardId", loginBean.getData().getUserCardId());
                                            SaveKey("UserInfo", "token", loginBean.getToken());
                                        } else if (loginBean.getStatus().equals("userNoExist")) {
                                            TipDialog.show("用户不存在！", WaitDialog.TYPE.ERROR);
                                        } else if (loginBean.getStatus().equals("userPasswordError")) {
                                            MessageDialog.show("你的密码修改了", "请重新登录！😊", "确定")
                                                    .setOkTextInfo(new TextInfo().setFontColor(getResources().getColor(R.color.dialogxIOSBlue)))
                                                    //.setTitleIcon(R.drawable.ic_incorrect)
                                                    .setOkButton(new OnDialogButtonClickListener<MessageDialog>() {
                                                        @Override
                                                        public boolean onClick(MessageDialog baseDialog, View v) {
                                                            Intent intent = new Intent(activity, LoginActivity.class);
                                                            startActivity(intent);
                                                            return false;
                                                        }
                                                    });
                                        }
                                    }
                                });
                            }
                            successInterface.OnSuccess(json);
                        }
                    });
                }
            }
        });
    }

    /**
     * OKhttp封装(json参数传递方式)无加载动画
     */
    public static void OKJsonGet(Activity activity, String json, String url, OkSuccessInterface successInterface) {
        //创建OkHttpClient请求对象
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)//设置连接超时时间
                .readTimeout(60, TimeUnit.SECONDS)//设置读取超时时间
                .build();
        //创建一个RequestBody(参数1：数据类型 参数2传递的json串)
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), json);
        String token = "A";
        try {
            if (GetToken() != null)
                token = GetToken();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //创建一个请求对象
        Request request = new Request.Builder()
                .url(url)
                .addHeader("token", token)
                .get()
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @SuppressLint("CheckResult")
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                assert activity != null;
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity.getApplicationContext(), "服务器无响应", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (successInterface != null) {
                    String json = Objects.requireNonNull(response.body()).string();
                    assert activity != null;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            successInterface.OnSuccess(json);
                        }
                    });
                }
            }
        });
    }

    public static String OkHttpGet(String url) {
        OkHttpClient client = new OkHttpClient();
        try {
            client.newBuilder().connectTimeout(10000, TimeUnit.MILLISECONDS);
            Request request = new Request.Builder().url(url).build();
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            } else {
                throw new IOException("Unexpected code " + response);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 存储信息
     *
     * @param Mode Mode
     * @param key  键值
     * @param text 存储的内容
     */
    public static void SaveKey(String Mode, String key, String text) {
        SharedPreferences preferences = context.getSharedPreferences(Mode, Context.MODE_PRIVATE);
        preferences.edit().putString(key, text).apply();
    }

    /**
     * 获取存储信息
     *
     * @param Mode Mode
     * @param key  键值
     */
    public static String ShareKey(String Mode, String key) {
        SharedPreferences preferences = context.getSharedPreferences(Mode, Context.MODE_PRIVATE);
        String text = preferences.getString(key, null);
        return text;
    }

    public static User GetUserInfo() {
        String userinfo = ShareKey("UserInfo", "info");
        if (userinfo != null) {
            Login login = new Gson().fromJson(userinfo, Login.class);
            return login.getData();
        } else {
            return null;
        }
    }

    public static String GetAppData() {
        String isFromLogout = ShareKey("UserInfo", "isFromLogout");
        if (isFromLogout != null) {
            return isFromLogout;
        } else {
            return null;
        }
    }

    public static String GetUserCardId() {
        String isFromLogout = ShareKey("UserInfo", "userCardId");
        if (isFromLogout != null) {
            return isFromLogout;
        } else {
            return null;
        }
    }

    public static String GetToken() {
        String token = ShareKey("UserInfo", "token");
        if (token != null) {
            return token;
        } else {
            return null;
        }
    }

    /**
     * 手机号号段校验，
     * 第1位：1；
     * 第2位：{3、4、5、6、7、8}任意数字；
     * 第3—11位：0—9任意数字
     * 规则：第一位只能是1，第二位为3-8中的数字，3-11位为任意的数字
     *
     * @param value
     * @return
     */
    public static boolean isTelPhoneNumber(String value) {
        if (value != null && value.length() == 11) {
            Pattern pattern = Pattern.compile("^1[3|4|5|6|7|8][0-9]\\d{8}$");
            Matcher matcher = pattern.matcher(value);
            return matcher.matches();
        }
        return false;
    }

    /**
     * 验证输入的名字是否为“中文”或者是否包含“·”
     * <p>
     * 验证规则是：姓名由汉字或汉字加“•”、"·"组成，并且，“点”只能有一个，“点”的位置不能在首位也不能在末尾，只有在汉字之间才会验证经过。
     */
    public static boolean isLegalName(String name) {
        if (name.contains("·") || name.contains("•")) {
            if (name.matches("^[\\u4e00-\\u9fa5]+[·•][\\u4e00-\\u9fa5]+$")) {
                return true;
            } else {
                return false;
            }
        } else {
            if (name.matches("^[\\u4e00-\\u9fa5]+$")) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 验证输入的身份证号是否合法
     * <p>
     * 规则是：由15位数字或18位数字（17位数字加“x”）组成，15位纯数字没什么好说的，18位的话，能够是18位纯数字，或者17位数字加“x”
     */
    public static boolean isLegalId(String id) {
        if (id.toUpperCase().matches("(^\\d{15}$)|(^\\d{17}([0-9]|X)$)")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 邮箱验证
     */
    public static boolean isEmail(String strEmail) {
        String strPattern = "^[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
        if (TextUtils.isEmpty(strPattern)) {
            return false;
        } else {
            return strEmail.matches(strPattern);
        }
    }

    /**
     * 获取当前日期：yyyy-MM-dd hh:mm:ss
     *
     * @return String
     */
    public static String getCurrentTime(String format) {
        return new SimpleDateFormat(format).format(new Date());
    }

    /**
     * 获取当前日期的后几天：yyyy-MM-dd hh:mm:ss
     *
     * @return String
     */
    public static String getTodayAfterday(String format, int day) {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, day);
        String time = "";
        time = dateFormat.format(calendar.getTime());
        return time;
    }


    //使用默认浏览器打开链接
    public boolean openUrl(String url) {
        try {
            Uri uri = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            context.startActivity(intent);
            return true;
        } catch (Exception e) {
            if (DEBUGMODE) {
                e.printStackTrace();
            }
            return false;
        }
    }

    //打开指定App
    public boolean openApp(String packageName) {
        PackageManager packageManager = getPackageManager();
        if (isInstallApp(packageName)) {
            try {
                Intent intent = packageManager.getLaunchIntentForPackage(packageName);
                context.startActivity(intent);
                return true;
            } catch (Exception e) {
                if (DEBUGMODE) {
                    e.printStackTrace();
                }
                return false;
            }
        } else {
            return false;
        }
    }

    //检测App是否已安装
    public boolean isInstallApp(String packageName) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packageInfo != null;
    }

    /**
     * 毫秒时间戳转日期String
     */
    public static String toDate(long time) {
        //当前时间毫秒的时间戳转换为日期
        Date millisecondDate = new Date(time);
        //格式化时间
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String millisecondStrings = formatter.format(millisecondDate);
        return millisecondStrings;
    }

}

